package com.dsteam.commune.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dsteam.commune.R;
import com.dsteam.commune.activities.PollActivity;
import com.dsteam.commune.activities.TaskActivity;
import com.dsteam.commune.model.FeedObject;
import com.dsteam.commune.model.Poll;
import com.dsteam.commune.model.Task;

/**
 * Created by march_000 on 27.11.2014.
 */
public class FeedAdapter extends ArrayAdapter<FeedObject> {

    class ViewHolder{
        public ViewHolder(View rootView){

        }
    }

    Context mContext;

    public FeedAdapter(Context context, FeedObject[] data) {
        super(context, R.layout.feed_item_poll, data);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            switch (getItemViewType(position)) {
                case 0: {
                    final Poll poll = (Poll)getItem(position);
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.feed_item_poll, parent, false);
                    ((TextView)convertView.findViewById(R.id.feed_item_poll_text)).setText(poll.getQuestion());
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(mContext, PollActivity.class);
                            intent.putExtra(PollActivity.POLLUSERIDSTRING, poll.getPollUserID());
                            intent.putExtra(PollActivity.POLLIDSTRING, poll.getPollID());
                            mContext.startActivity(intent);
                        }
                    });
                }
                break;
                case 1:{
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.feed_item_task, parent, false);
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(mContext, TaskActivity.class);
                            mContext.startActivity(intent);
                        }
                    });
                }
                break;
            }
        }

        if(position%2==0){
            convertView.setBackground(mContext.getResources().getDrawable(R.drawable.bottom_connect_selector));
        }else {
            convertView.setBackground(mContext.getResources().getDrawable(R.drawable.top_connect_selector));
        }

        return convertView;

    }

    @Override
    public int getViewTypeCount() {
        return 2;//change this
    }

    @Override
    public int getItemViewType(int position) {
        if(getItem(position) instanceof Poll) return 0;
        else if(getItem(position) instanceof Task) return 1;
        else return 0;
    }



}
