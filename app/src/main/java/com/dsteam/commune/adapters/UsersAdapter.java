package com.dsteam.commune.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dsteam.commune.R;
import com.dsteam.commune.activities.ActiveNFCJoin;
import com.dsteam.commune.model.User;

/**
 * Created by march_000 on 05.12.2014.
 */
public class UsersAdapter extends ArrayAdapter<User> {

    class ViewHolder{
        public ViewHolder(View view){
            usernameText = (TextView)view.findViewById(R.id.users_item_username);
        }
        public TextView usernameText;
    }


    Context mContext;
    public UsersAdapter(Context context, User[] data) {
        super(context, R.layout.users_item, data);
        mContext = context;
    }

    @Override
    public User getItem(int position) {
        if(position==getCount()-1)return null;
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            if(getItemViewType(position)==0)
                convertView = LayoutInflater.from(mContext).inflate(R.layout.users_item, parent, false);
            else
                convertView = LayoutInflater.from(mContext).inflate(R.layout.users_item_plus, parent, false);
            convertView.setTag(new ViewHolder(convertView));
        }
        if(position%2==0)
            convertView.setBackgroundColor(mContext.getResources().getColor(R.color.commune_light_grey));
        else
            convertView.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        if(position!=getCount()-1) {
            ((ViewHolder) convertView.getTag()).usernameText.setText(getItem(position).getUsername());//TODO: set to username
        }
        else
        {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, ActiveNFCJoin.class);
                    mContext.startActivity(intent);
                }
            });
        }
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 2;//change this
    }

    @Override
    public int getItemViewType(int position) {
        if(position==super.getCount())return 1;
        else return 0;
    }


    @Override
    public int getCount() {
        return super.getCount()+1;
    }
}
