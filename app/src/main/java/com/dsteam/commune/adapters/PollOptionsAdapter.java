package com.dsteam.commune.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dsteam.commune.R;
import com.dsteam.commune.activities.PollActivity;
import com.dsteam.commune.logic.CommuneLogic;
import com.dsteam.commune.logic.ICommuneLogic;
import com.dsteam.commune.model.PollOption;
import com.dsteam.commune.model.User;

/**
 * Created by march_000 on 05.12.2014.
 */
public class PollOptionsAdapter extends ArrayAdapter<PollOption> {
    PollActivity mContext;

    class ViewHolder{
        public ViewHolder(View view) {
            posView = view.findViewById(R.id.poll_option_pos);
            negView = view.findViewById(R.id.poll_option_neg);
            mainOptionView = (TextView) view.findViewById(R.id.poll_option_text);
            secondaryOptionView = (TextView) view.findViewById(R.id.poll_option_text_ussernames);
        }
        View posView;
        View negView;
        TextView mainOptionView;
        TextView secondaryOptionView;
    }

    public PollOptionsAdapter(Context context, PollOption[] data) {
        super(context, R.layout.users_item, data);
        mContext = (PollActivity)context;
    }

    int max = -1;
    int getMax(){
        if(max==-1) {
            for(int i=0; i<getCount(); i++){
                max = Math.max(max, getItem(i).getNumberOfVotes());
            }
        }
        return max;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.poll_item_option, parent, false);
            convertView.setTag(new ViewHolder(convertView));
        }
        ViewHolder viewHolder = (ViewHolder)convertView.getTag();

        float posvalue = getItem(position).getNumberOfVotes();
        float negvalue = getMax() - posvalue;
        LinearLayout.LayoutParams posParam = new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.MATCH_PARENT, posvalue);
        LinearLayout.LayoutParams negParam = new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.MATCH_PARENT, negvalue);


        final PollOption pollOption = getItem(position);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ICommuneLogic communeLogic = CommuneLogic.getInstance(mContext);
                    Log.v(getClass().toString(), "trying to vote for "+pollOption.getOptionID());
                    if(!pollOption.hasVoted(communeLogic.getOwnUser())) {
                        communeLogic.vote(pollOption.vote());
                        mContext.updateView();
                    }
                }
            });

        viewHolder.negView.setLayoutParams(negParam);
        viewHolder.posView.setLayoutParams(posParam);
        viewHolder.mainOptionView.setText(
                getItem(position).getOption()
                        +": "+getItem(position).getNumberOfVotes()+" "
                        + mContext.getString(R.string.vote_name));



        String secondaryString ="";
        if(pollOption.getVoters().isEmpty())secondaryString = mContext.getString(R.string.no_voter_secondary_text);
        else {
            for (User user : pollOption.getVoters()) {
                secondaryString += user.getUsername() + " / ";
            }
            secondaryString = secondaryString.substring(0, secondaryString.length()-3);
        }

        viewHolder.secondaryOptionView.setText(secondaryString
        );


        if(position%2==0){
            convertView.setBackgroundColor(mContext.getResources().getColor(R.color.commune_light_grey));
        }else {
            convertView.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }

        return convertView;
    }
}
