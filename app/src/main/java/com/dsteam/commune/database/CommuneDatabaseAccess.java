package com.dsteam.commune.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.dsteam.commune.model.Poll;
import com.dsteam.commune.model.PollOption;
import com.dsteam.commune.model.PollVote;
import com.dsteam.commune.model.Timestamp;
import com.dsteam.commune.model.User;
import com.dsteam.commune.model.VectorClock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by Cedric on 12/2/2014.
 */
public class CommuneDatabaseAccess {

    private final static String LOG_TAG = CommuneDatabaseAccess.class.getSimpleName();

    CommuneDatabaseHelper mDbHelper;

    public CommuneDatabaseAccess(Context context) {
        mDbHelper = new CommuneDatabaseHelper(context);
    }

    public void onClose(){
        //call this for closing the database connection
        mDbHelper.close();
    }

    //GETTERS MY USER

    public int getMyClockEntry(){
        //gets the user's current clock entry
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String[] projection = {CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY};
        Cursor c = db.query(CommuneContract.MyUser.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
                );
        c.moveToFirst();

        if(c.getCount() ==0){
            c.close();
            return -1;
        }

        int clockEntry = c.getInt(c.getColumnIndex(CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY));
        c.close();
        return clockEntry;
    }

    public int getAndUpdateMyClockEntry() {
        int clock = getMyClockEntry();
        // todo: update clockEntry (+1)
        return clock;
    }

    public String getMyUserId(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String[] projection = {CommuneContract.MyUser.COLUMN_NAME_MY_USER_ID};
        Cursor c = db.query(CommuneContract.MyUser.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        c.moveToFirst();

        if(c.getCount() == 0){
            c.close();
            return null;
        }

        String userId = c.getString(c.getColumnIndex(CommuneContract.MyUser.COLUMN_NAME_MY_USER_ID));
        c.close();
        return userId;
    }

    public String getMyUserName(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String[] projection = {CommuneContract.MyUser.COLUMN_NAME_USERNAME};
        Cursor c = db.query(CommuneContract.MyUser.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        c.moveToFirst();

        if(c.getCount() == 0){
            c.close();
            return null;
        }

        String userId = c.getString(c.getColumnIndex(CommuneContract.MyUser.COLUMN_NAME_USERNAME));
        c.close();
        return userId;
    }

    public User getMyUser() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        //first get my user id
        Cursor c = db.query(CommuneContract.MyUser.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );
        c.moveToFirst();

        if(c.getCount() == 0){
            return null;
        }

        String myUserId = c.getString(c.getColumnIndex(CommuneContract.MyUser.COLUMN_NAME_MY_USER_ID));

        c.close();

        String selection = CommuneContract.User.COLUMN_NAME_MEMBER_USER_ID + " = \"" + myUserId + "\"";
        c = db.query(CommuneContract.User.TABLE_NAME,
                null,
                selection,
                null,
                null,
                null,
                null
        );
        if(c.getCount() < 1){
            //return null because we are not in a group yet
            c.close();
            return null;
        }
        c.moveToFirst();
        String myGcmId = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_MEMBER_GCM_ID));
        String myUsername = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_USERNAME));
        String adderUserId = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_USER_ID));
        int adderClockEntry = c.getInt(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_CLOCK_ENTRY));

        c.close();
        return new User(myGcmId, myUsername, myUserId, new Timestamp(adderUserId,adderClockEntry));
    }

    public VectorClock getMyVectorClock(){
        //gets the user's current vector clock

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        HashMap<String, Integer> vectorClock = new HashMap();
        String userID;
        Integer clockEntry;

        String[] projection = {CommuneContract.User.COLUMN_NAME_MEMBER_USER_ID};

        Cursor c = db.query(CommuneContract.User.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        c.moveToFirst();
        projection[0] = "max("+CommuneContract.Poll.COLUMN_NAME_CLOCK_ENTRY+") as MAXCLOCK";
        String selection;
        Cursor c2;

        int size=c.getCount();
        for(int i = 0;i<size;i++){
            userID = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_MEMBER_USER_ID));
            c.moveToNext ();
            selection = CommuneContract.Poll.COLUMN_NAME_POLL_USER_ID + " = \"" + userID + "\"";
            clockEntry = 0;

            c2 = db.query(CommuneContract.Poll.TABLE_NAME,
                    projection,
                    selection,
                    null,
                    null,
                    null,
                    null
            );
            if(c2.getCount() == 1) {
                c2.moveToFirst();
                clockEntry = Math.max(clockEntry, c2.getInt(c2.getColumnIndex("MAXCLOCK")));
            }
            c2.close();

            c2 = db.query(CommuneContract.PollVote.TABLE_NAME,
                    projection,
                    selection,
                    null,
                    null,
                    null,
                    null
            );
            if(c2.getCount() == 1) {
                c2.moveToFirst();
                clockEntry = Math.max(clockEntry, c2.getInt(c2.getColumnIndex("MAXCLOCK")));
            }
            c2.close();

            c2 = db.query(CommuneContract.ToDo.TABLE_NAME,
                    projection,
                    selection,
                    null,
                    null,
                    null,
                    null
            );
            if(c2.getCount() == 1) {
                c2.moveToFirst();
                clockEntry = Math.max(clockEntry, c2.getInt(c2.getColumnIndex("MAXCLOCK")));
            }

            c2.close();

            c2 = db.query(CommuneContract.User.TABLE_NAME,
                    projection,
                    selection,
                    null,
                    null,
                    null,
                    null
            );
            if(c2.getCount() == 1) {
                c2.moveToFirst();
                clockEntry = Math.max(clockEntry, c2.getInt(c2.getColumnIndex("MAXCLOCK")));
            }
            c2.close();

            vectorClock.put(userID, clockEntry);

        }
        c.close();
        return new VectorClock(vectorClock);
    }

    //GETTERS POLL

    public List<Poll> getAllPolls(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        ArrayList<Poll> pollList = new ArrayList<Poll>();

        Cursor c = db.query(CommuneContract.Poll.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );
        c.moveToFirst();
        int i = c.getCount();
        for(int j=0;j<i;j++){
            String selection = CommuneContract.PollOption.COLUMN_NAME_POLL_ID + " = " + c.getString(c.getColumnIndex(CommuneContract.Poll.COLUMN_NAME_CLOCK_ENTRY)) + " AND " +CommuneContract.PollOption.COLUMN_NAME_POLL_USER_ID + " = \"" + c.getString(c.getColumnIndex(CommuneContract.Poll.COLUMN_NAME_POLL_USER_ID)) + "\"";
            Cursor c2 = db.query(CommuneContract.PollOption.TABLE_NAME,
                    null,
                    selection,
                    null,
                    null,
                    null,
                    null
            );
            c2.moveToFirst();
            pollList.add(new Poll(c, c2));
            c2.close();
            c.moveToNext();
        }
        c.close();
        return pollList;
    }

    public List<Poll> getAllMissingPolls(VectorClock vectorclock){
        //get our vectorclock
        VectorClock myVectorClock = this.getMyVectorClock();


        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        ArrayList<Poll> pollList = new ArrayList<Poll>();
        Set<String> keySet = myVectorClock.getClockEntries().keySet();
        for(String userID : keySet){
            Integer clockEntry = vectorclock.getClockEntries().get(userID);
            if(clockEntry == null){
                clockEntry = 0;
            }

            //get all the polls with a higher clock entry than vectorclock
            String selection = CommuneContract.Poll.COLUMN_NAME_POLL_USER_ID + " = \"" + userID + "\" AND " + CommuneContract.Poll.COLUMN_NAME_CLOCK_ENTRY + " > " + clockEntry.toString();
            Cursor c = db.query(CommuneContract.Poll.TABLE_NAME,
                    null,
                    selection,
                    null,
                    null,
                    null,
                    null
            );

            c.moveToFirst();
            int i = c.getCount();
            for(int j=0;j<i;j++){
                //add all the polls

                selection = CommuneContract.PollOption.COLUMN_NAME_POLL_ID + " = " + c.getString(c.getColumnIndex(CommuneContract.Poll.COLUMN_NAME_CLOCK_ENTRY)) + " AND " +CommuneContract.PollOption.COLUMN_NAME_POLL_USER_ID + " = \"" + c.getString(c.getColumnIndex(CommuneContract.Poll.COLUMN_NAME_POLL_USER_ID)) + "\"";
                Cursor c2 = db.query(CommuneContract.PollOption.TABLE_NAME,
                        null,
                        selection,
                        null,
                        null,
                        null,
                        null
                );
                c2.moveToFirst();
                pollList.add(new Poll(c, c2));
                c2.close();
                c.moveToNext();
            }

            c.close();
        }
        return pollList;
    }

    public Poll getPoll(String pollUserID, int pollID) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String selection = CommuneContract.Poll.COLUMN_NAME_POLL_USER_ID + " = \"" + pollUserID + "\" AND " + CommuneContract.Poll.COLUMN_NAME_CLOCK_ENTRY + " = " + pollID;

        Cursor c = db.query(CommuneContract.Poll.TABLE_NAME,
                null,
                selection,
                null,
                null,
                null,
                null
        );
        c.moveToFirst();

        if(c.getCount() == 1) {
            //if there is an entry we return it
            selection = CommuneContract.PollOption.COLUMN_NAME_POLL_ID + " = " + c.getString(c.getColumnIndex(CommuneContract.Poll.COLUMN_NAME_CLOCK_ENTRY)) + " AND " + CommuneContract.PollOption.COLUMN_NAME_POLL_USER_ID + " = \"" + c.getString(c.getColumnIndex(CommuneContract.Poll.COLUMN_NAME_POLL_USER_ID)) + "\"";
            Cursor c2 = db.query(CommuneContract.PollOption.TABLE_NAME,
                    null,
                    selection,
                    null,
                    null,
                    null,
                    null
            );
            c2.moveToFirst();
            Poll p = new Poll(c, c2);
            c.close();
            c2.close();

            Log.e(LOG_TAG, "Got Poll with question" + p.getQuestion());

            return p;
        }
        return null;
    }

    //GETTERS USER

    public List<User> getAllUsers() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        //get all users
        Cursor c = db.query(CommuneContract.User.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );

        c.moveToFirst();
        int i = c.getCount();
        ArrayList<User> userList = new ArrayList<User>();
        for(int j=0;j<i;j++){
            userList.add(new User(c));
            c.moveToNext();
        }
        c.close();
        return userList;
    }

    public List<User> getAllMissingUsers(VectorClock vectorclock){
        //get our vectorclock
        VectorClock myVectorClock = this.getMyVectorClock();


        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        ArrayList<User> userList = new ArrayList<User>();
        Set<String> keySet = myVectorClock.getClockEntries().keySet();
        for(String userID : keySet){
            Integer clockEntry = vectorclock.getClockEntries().get(userID);
            if(clockEntry == null){
                clockEntry = 0;
            }

            //get all the users with a higher clock entry than vectorclock
            String selection = CommuneContract.User.COLUMN_NAME_USER_ID + " = \"" + userID + "\" AND " + CommuneContract.User.COLUMN_NAME_CLOCK_ENTRY + " > " + clockEntry.toString();
            Cursor c = db.query(CommuneContract.User.TABLE_NAME,
                    null,
                    selection,
                    null,
                    null,
                    null,
                    null
            );

            c.moveToFirst();
            int i = c.getCount();
            for(int j=0;j<i;j++){
                User user = new User(c);
                userList.add(user);
                c.moveToNext();
            }

            c.close();
        }
        return userList;
    }

    public List<User> getAllOtherUsers(){
        // return all users in the group, except myUser
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        //first get my user id
        Cursor c = db.query(CommuneContract.MyUser.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );
        c.moveToFirst();

        if(c.getCount() == 0){
            c.close();
            return null;
        }

        String myUserId = c.getString(c.getColumnIndex(CommuneContract.MyUser.COLUMN_NAME_MY_USER_ID));

        c.close();
        //get all users except myUser
        String selection = CommuneContract.User.COLUMN_NAME_MEMBER_USER_ID + " != \"" + myUserId + "\"";
        c = db.query(CommuneContract.User.TABLE_NAME,
                null,
                selection,
                null,
                null,
                null,
                null
        );

        c.moveToFirst();
        int i = c.getCount();
        ArrayList<User> userList = new ArrayList<User>();
        for(int j=0;j<i;j++){
            userList.add(new User(c));
            c.moveToNext();
        }
        c.close();
        return userList;
    }

    public User getUserByID(String userID) {

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String selection = CommuneContract.User.COLUMN_NAME_MEMBER_USER_ID + " = \"" + userID +"\"";
        Cursor c = db.query(CommuneContract.User.TABLE_NAME,
                null,
                selection,
                null,
                null,
                null,
                null
        );
        if(c.getCount() != 1){
            //return null because we don't have this user ID
            c.close();
            return null;
        }
        c.moveToFirst();
        String GcmId = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_MEMBER_GCM_ID));
        String Username = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_USERNAME));
        String adderUserId = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_USER_ID));
        int adderClockEntry = c.getInt(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_CLOCK_ENTRY));

        c.close();
        return new User(GcmId, Username, userID, new Timestamp(adderUserId,adderClockEntry));
    }

    public List<User> getUsersByListOfIDs(List<String> userIDs) {
        List<User> usersList = new ArrayList<User>();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        for(String id : userIDs) {
            String selection = CommuneContract.User.COLUMN_NAME_MEMBER_USER_ID + " = \"" + id + "\"";
            Cursor c = db.query(CommuneContract.User.TABLE_NAME,
                    null,
                    selection,
                    null,
                    null,
                    null,
                    null
            );
            if(c.getCount() == 1) {
                //we have found the user and add it to the list

                c.moveToFirst();
                String GcmId = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_MEMBER_GCM_ID));
                String Username = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_USERNAME));
                String adderUserId = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_USER_ID));
                int adderClockEntry = c.getInt(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_CLOCK_ENTRY));

                c.close();
                usersList.add(new User(GcmId, Username, id, new Timestamp(adderUserId, adderClockEntry)));
            }
            else{
                c.close();
            }
        }

        return usersList;
    }

    //GETTERS POLLVOTE

    public List<PollVote> getAllPollVotes(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        //get all PollVotes
        Cursor c = db.query(CommuneContract.PollVote.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );

        c.moveToFirst();
        int i = c.getCount();
        ArrayList<PollVote> pollVoteList = new ArrayList<PollVote>();
        for(int j=0;j<i;j++){
            pollVoteList.add(new PollVote(c));
            c.moveToNext();
        }
        c.close();
        for(PollVote vote: pollVoteList){
            Log.e(LOG_TAG, "Got PollVote for optionID" + vote.getOptionID());
        }
        return pollVoteList;
    }

    public List<PollVote> getAllMissingPollVotes(VectorClock vectorclock){
        //get our vectorclock
        VectorClock myVectorClock = this.getMyVectorClock();


        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        ArrayList<PollVote> pollVoteList = new ArrayList<PollVote>();
        Set<String> keySet = myVectorClock.getClockEntries().keySet();
        for(String userID : keySet){
            Integer clockEntry = vectorclock.getClockEntries().get(userID);
            if(clockEntry == null){
                clockEntry = 0;
            }

            //get all the pollVotes with a higher clock entry than vectorclock
            String selection = CommuneContract.PollVote.COLUMN_NAME_USER_ID + " = \"" + userID + "\" AND " + CommuneContract.PollVote.COLUMN_NAME_CLOCK_ENTRY + " > " + clockEntry.toString();
            Cursor c = db.query(CommuneContract.PollVote.TABLE_NAME,
                    null,
                    selection,
                    null,
                    null,
                    null,
                    null
            );

            c.moveToFirst();
            int i = c.getCount();
            for(int j=0;j<i;j++){
                PollVote pollVote = new PollVote(c);
                pollVoteList.add(pollVote);
                c.moveToNext();
            }
            c.close();

        }
        return pollVoteList;
    }

    public List<PollVote> getPollVotes(PollOption pollOption){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String selection = CommuneContract.PollOption.COLUMN_NAME_POLL_ID + " = " + pollOption.getPollID() + " AND " + CommuneContract.PollVote.COLUMN_NAME_POLL_USER_ID + " = \"" + pollOption.getPollUserID() +"\" AND " + CommuneContract.PollVote.COLUMN_NAME_OPTION_ID +" = " + pollOption.getOptionID();
        Cursor c = db.query(CommuneContract.PollVote.TABLE_NAME,
                null,
                selection,
                null,
                null,
                null,
                null
        );
        c.moveToFirst();
        int i = c.getCount();
        ArrayList<PollVote> pollVoteList = new ArrayList<PollVote>();
        for(int j=0;j<i;j++){
            pollVoteList.add(new PollVote(c));
            c.moveToNext();
        }
        for(PollVote vote: pollVoteList){
            Log.e(LOG_TAG, "Got PollVote for optionID " + vote.getOptionID());
        }
        c.close();
        return pollVoteList;
    }


    //INSERTION METHODS

    public long setUpMyUser(String username, String userid, String gcmid){
        //only call this method once in the beginning

        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        //get MAC address as user ID

        ContentValues values = new ContentValues();
        values.put(CommuneContract.MyUser.COLUMN_NAME_USERNAME, username);
        values.put(CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY, 0);
        values.put(CommuneContract.MyUser.COLUMN_NAME_MY_USER_ID, userid);
        values.put(CommuneContract.MyUser.COLUMN_NAME_MY_GCM_ID, gcmid);

        //insert the values
        long newRowId;
        newRowId = db.insert(
                CommuneContract.MyUser.TABLE_NAME,
                null,
                values);



        //now we insert our user into the user table
        String[] projection = {CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY, CommuneContract.MyUser.COLUMN_NAME_MY_USER_ID};
        Cursor c = db.query(CommuneContract.MyUser.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        c.moveToFirst();

        //get our clock entry and userID
        int myClockEntry = c.getInt(c.getColumnIndex(CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY));

        String myUserID = c.getString(c.getColumnIndex(CommuneContract.MyUser.COLUMN_NAME_MY_USER_ID));

        c.close();
        //update our clock entry
        values = new ContentValues();
        values.put(CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY,myClockEntry);
        db.update(CommuneContract.MyUser.TABLE_NAME,values,null,null);

        //insert the user
        ContentValues cv = new ContentValues();
        cv.put(CommuneContract.User.COLUMN_NAME_USER_ID,myUserID);
        cv.put(CommuneContract.User.COLUMN_NAME_CLOCK_ENTRY, myClockEntry);
        cv.put(CommuneContract.User.COLUMN_NAME_USERNAME, username);
        cv.put(CommuneContract.User.COLUMN_NAME_MEMBER_USER_ID, userid);
        cv.put(CommuneContract.User.COLUMN_NAME_MEMBER_GCM_ID, gcmid);

        newRowId = db.insert(
                CommuneContract.User.TABLE_NAME,
                null,
                cv);

        if (newRowId == -1) Log.e(LOG_TAG, "There was a problem creating a new user.");

        Log.e(LOG_TAG, "Created new user username: " + username);

        return newRowId;
    }

    public void insertAllMissingPollVotes(List<PollVote> pollVoteList){
        //this method doesn't care about the vectorClockEntry, it will insert everything anyway
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        for(PollVote pollVote : pollVoteList) {
            ContentValues values = new ContentValues();
            values.put(CommuneContract.PollVote.COLUMN_NAME_OPTION_ID, pollVote.getOptionID());
            values.put(CommuneContract.PollVote.COLUMN_NAME_POLL_USER_ID, pollVote.getPollUserID());
            values.put(CommuneContract.PollVote.COLUMN_NAME_POLL_ID, pollVote.getPollID());
            values.put(CommuneContract.PollVote.COLUMN_NAME_CLOCK_ENTRY, pollVote.timestamp.vectorClockEntry);
            values.put(CommuneContract.PollVote.COLUMN_NAME_USER_ID, pollVote.timestamp.userID);

            long newRowId;
            newRowId = db.insert(
                    CommuneContract.PollVote.TABLE_NAME,
                    null,
                    values);
            Log.e(LOG_TAG, "Inserted pollVote: " + pollVote.toString());


            if (newRowId == -1) Log.e(LOG_TAG, "There was a problem inserting a new pollVote.");
        }
    }

    public void insertAllMissingUsers(List<User> userList){
        //this method doesn't care about the vectorClockEntry, it will insert everything anyway

        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        for(User u : userList) {
            ContentValues values = new ContentValues();
            values.put(CommuneContract.User.COLUMN_NAME_MEMBER_GCM_ID, u.getGcmID());
            values.put(CommuneContract.User.COLUMN_NAME_MEMBER_USER_ID, u.getUserID());
            values.put(CommuneContract.User.COLUMN_NAME_USERNAME,u.getUsername());
            values.put(CommuneContract.User.COLUMN_NAME_CLOCK_ENTRY,u.timestamp.vectorClockEntry);
            values.put(CommuneContract.User.COLUMN_NAME_USER_ID,u.timestamp.userID);

            long newRowId;
            newRowId = db.insertWithOnConflict(
                    CommuneContract.User.TABLE_NAME,
                    null,
                    values,
                    SQLiteDatabase.CONFLICT_REPLACE
            );

            Log.e(LOG_TAG, "Inserted user " + u.getUsername());

            if (newRowId == -1) Log.e(LOG_TAG, "There was a problem inserting a new user.");
        }
    }

    public void insertAllMissingPolls(List<Poll> pollList){
        //this method doesn't care about the vectorClockEntry, it will insert everything anyway
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        for(Poll poll : pollList) {
            PollOption polloption;

            ContentValues values = new ContentValues();
            values.put(CommuneContract.Poll.COLUMN_NAME_QUESTION, poll.getQuestion());
            values.put(CommuneContract.Poll.COLUMN_NAME_POLL_USER_ID, poll.getPollUserID());
            values.put(CommuneContract.Poll.COLUMN_NAME_CLOCK_ENTRY, poll.timestamp.vectorClockEntry);

            long newRowId;
            newRowId = db.insert(
                    CommuneContract.Poll.TABLE_NAME,
                    null,
                    values);

            if (newRowId == -1) Log.e(LOG_TAG, "There was a problem inserting a new poll.");

            //insert the poll options
            for (int i = 0; i < poll.getOptions().size(); i++) {
                polloption = poll.getOptions().get(i);
                values = new ContentValues();
                values.put(CommuneContract.PollOption.COLUMN_NAME_POLL_ID, polloption.getPollID());
                values.put(CommuneContract.PollOption.COLUMN_NAME_OPTION, polloption.getOption());
                values.put(CommuneContract.PollOption.COLUMN_NAME_OPTION_ID, polloption.getOptionID());
                values.put(CommuneContract.PollOption.COLUMN_NAME_POLL_USER_ID, polloption.getPollUserID());

                newRowId = db.insert(
                        CommuneContract.PollOption.TABLE_NAME,
                        "null",
                        values);

                Log.e(LOG_TAG, "Inserted poll " + poll.getQuestion());

                if (newRowId == -1)
                    Log.e(LOG_TAG, "There was a problem inserting a new pollOption.");
            }
        }
    }

    public Poll createPoll(String question, List<String> options) {
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String[] projection = {CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY, CommuneContract.MyUser.COLUMN_NAME_MY_USER_ID};
        Cursor c = db.query(CommuneContract.MyUser.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        c.moveToFirst();

        //get our clock entry and userID
        int myClockEntry = c.getInt(c.getColumnIndex(CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY)) + 1;

        String myUserID = c.getString(c.getColumnIndex(CommuneContract.MyUser.COLUMN_NAME_MY_USER_ID));

        c.close();
        //update our clock entry
        ContentValues values = new ContentValues();
        values.put(CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY,myClockEntry);
        db.update(CommuneContract.MyUser.TABLE_NAME,values,null,null);

        //insert the poll
        ContentValues cv = new ContentValues();
        cv.put(CommuneContract.Poll.COLUMN_NAME_CLOCK_ENTRY, myClockEntry);
        cv.put(CommuneContract.Poll.COLUMN_NAME_POLL_USER_ID, myUserID);
        cv.put(CommuneContract.Poll.COLUMN_NAME_QUESTION, question);

        long newRowId;
        newRowId = db.insert(
                CommuneContract.Poll.TABLE_NAME,
                null,
                cv);

        if (newRowId == -1) Log.e(LOG_TAG, "There was a problem creating a new poll.");

        Poll createdPoll = new Poll(question, myUserID, myClockEntry);

        int i = 0;
        for(String opt : options) {
            ContentValues optCv = new ContentValues();
            optCv.put(CommuneContract.PollOption.COLUMN_NAME_OPTION, opt);
            optCv.put(CommuneContract.PollOption.COLUMN_NAME_OPTION_ID, i);
            optCv.put(CommuneContract.PollOption.COLUMN_NAME_POLL_ID, myClockEntry);
            optCv.put(CommuneContract.PollOption.COLUMN_NAME_POLL_USER_ID, myUserID);

            db.insert(
                    CommuneContract.PollOption.TABLE_NAME,
                    null,
                    optCv);

            createdPoll.addOption(opt);

            i++;
        }

        Log.e(LOG_TAG, "Created new poll " + question);
        return createdPoll;
    }

    public User createUser(String username, String userId, String gcmId){
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String[] projection = {CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY, CommuneContract.MyUser.COLUMN_NAME_MY_USER_ID};
        Cursor c = db.query(CommuneContract.MyUser.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        c.moveToFirst();

        //get our clock entry and userID
        int myClockEntry = c.getInt(c.getColumnIndex(CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY)) + 1;

        String myUserID = c.getString(c.getColumnIndex(CommuneContract.MyUser.COLUMN_NAME_MY_USER_ID));

        c.close();
        //update our clock entry
        ContentValues values = new ContentValues();
        values.put(CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY,myClockEntry);
        db.update(CommuneContract.MyUser.TABLE_NAME,values,null,null);

        //insert the user
        ContentValues cv = new ContentValues();
        cv.put(CommuneContract.User.COLUMN_NAME_USER_ID,myUserID);
        cv.put(CommuneContract.User.COLUMN_NAME_CLOCK_ENTRY, myClockEntry);
        cv.put(CommuneContract.User.COLUMN_NAME_USERNAME, username);
        cv.put(CommuneContract.User.COLUMN_NAME_MEMBER_USER_ID, userId);
        cv.put(CommuneContract.User.COLUMN_NAME_MEMBER_GCM_ID, gcmId);

        long newRowId;
        newRowId = db.insert(
                CommuneContract.User.TABLE_NAME,
                null,
                cv);

        if (newRowId == -1) Log.e(LOG_TAG, "There was a problem creating a new user.");

        User createdUser = new User(gcmId,username,userId,new Timestamp(myUserID, myClockEntry));
        Log.e(LOG_TAG, "Created new user username: " + username);
        return createdUser;
    }

    public PollVote createPollVote(int pollId, String pollUserId, int optionId){
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String[] projection = {CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY, CommuneContract.MyUser.COLUMN_NAME_MY_USER_ID};
        Cursor c = db.query(CommuneContract.MyUser.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        c.moveToFirst();

        //get our clock entry and userID
        int myClockEntry = c.getInt(c.getColumnIndex(CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY)) + 1;

        String myUserID = c.getString(c.getColumnIndex(CommuneContract.MyUser.COLUMN_NAME_MY_USER_ID));

        c.close();
        //update our clock entry
        ContentValues values = new ContentValues();
        values.put(CommuneContract.MyUser.COLUMN_NAME_CLOCK_ENTRY,myClockEntry);
        db.update(CommuneContract.MyUser.TABLE_NAME,values,null,null);

        //insert our pollVote
        ContentValues cv = new ContentValues();
        cv.put(CommuneContract.PollVote.COLUMN_NAME_USER_ID, myUserID);
        cv.put(CommuneContract.PollVote.COLUMN_NAME_CLOCK_ENTRY, myClockEntry);
        cv.put(CommuneContract.PollVote.COLUMN_NAME_POLL_ID, pollId);
        cv.put(CommuneContract.PollVote.COLUMN_NAME_POLL_USER_ID, pollUserId);
        cv.put(CommuneContract.PollVote.COLUMN_NAME_OPTION_ID, optionId);

        long newRowId;
        newRowId = db.insert(
                CommuneContract.PollVote.TABLE_NAME,
                null,
                cv);

        if (newRowId == -1) Log.e(LOG_TAG, "There was a problem creating a new pollVote.");

        PollVote createdVote = new PollVote(myUserID, pollId, pollUserId, optionId, myClockEntry);

        Log.e(LOG_TAG, "Created new pollVote OptionId: " + optionId);

        return createdVote;
    }

    public boolean insertPoll(Poll poll){
        //get our vectorclock
        VectorClock myVectorClock = this.getMyVectorClock();

        Integer myClockEntry, messageClockEntry;
        messageClockEntry = poll.timestamp.vectorClockEntry;
        myClockEntry = myVectorClock.getClockEntries().get(poll.timestamp.userID);
        //return false if the user is not in our database
        if(myClockEntry == null){
            return false;
        }

        //return false if the message is not in the right order
        if((myClockEntry.intValue() + 1) != messageClockEntry.intValue()){
            return false;
        }

        //now we can insert the poll

        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        PollOption polloption;

        ContentValues values = new ContentValues();
        values.put(CommuneContract.Poll.COLUMN_NAME_QUESTION,poll.getQuestion());
        values.put(CommuneContract.Poll.COLUMN_NAME_POLL_USER_ID,poll.getPollUserID());
        values.put(CommuneContract.Poll.COLUMN_NAME_CLOCK_ENTRY,poll.timestamp.vectorClockEntry);

        long newRowId;
        newRowId = db.insert(
                CommuneContract.Poll.TABLE_NAME,
                null,
                values);

        if (newRowId == -1) Log.e(LOG_TAG, "There was a problem inserting a new poll.");

        //insert the poll options
        for(int i=0;i < poll.getOptions().size();i++){
            polloption = poll.getOptions().get(i);
            values = new ContentValues();
            values.put(CommuneContract.PollOption.COLUMN_NAME_POLL_ID, polloption.getPollID());
            values.put(CommuneContract.PollOption.COLUMN_NAME_OPTION, polloption.getOption());
            values.put(CommuneContract.PollOption.COLUMN_NAME_OPTION_ID, polloption.getOptionID());
            values.put(CommuneContract.PollOption.COLUMN_NAME_POLL_USER_ID, polloption.getPollUserID());

            newRowId = db.insert(
                    CommuneContract.PollOption.TABLE_NAME,
                    "null",
                    values);

            if (newRowId == -1) Log.e(LOG_TAG, "There was a problem inserting a new pollOption.");
        }
        Log.e(LOG_TAG, "Inserted poll " + poll.getQuestion());
        return true;
    }

    public boolean insertPollVote(PollVote pollVote) {
        //get our vectorclock
        VectorClock myVectorClock = this.getMyVectorClock();

        Integer myClockEntry, messageClockEntry;
        messageClockEntry = pollVote.timestamp.vectorClockEntry;
        myClockEntry = myVectorClock.getClockEntries().get(pollVote.timestamp.userID);
        //return false if the user is not in our database
        if(myClockEntry == null){
            return false;
        }

        //return false if the message is not in the right order
        if((myClockEntry.intValue() + 1) != messageClockEntry.intValue()){
            return false;
        }

        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String selection = CommuneContract.Poll.COLUMN_NAME_POLL_USER_ID + " = \"" + pollVote.getPollUserID() + "\" AND " + CommuneContract.Poll.COLUMN_NAME_CLOCK_ENTRY + " = " + pollVote.getPollID();

        Cursor c = db.query(CommuneContract.Poll.TABLE_NAME,
                null,
                selection,
                null,
                null,
                null,
                null
        );
        if(c.getCount() == 0) {
            //the poll for this pollvote is not in our database, so we are missing entries
            return false;
        }
        c.close();

        //now we can insert the pollVote
        ContentValues values = new ContentValues();
        values.put(CommuneContract.PollVote.COLUMN_NAME_OPTION_ID, pollVote.getOptionID());
        values.put(CommuneContract.PollVote.COLUMN_NAME_POLL_USER_ID, pollVote.getPollUserID());
        values.put(CommuneContract.PollVote.COLUMN_NAME_POLL_ID, pollVote.getPollID());
        values.put(CommuneContract.PollVote.COLUMN_NAME_CLOCK_ENTRY, pollVote.timestamp.vectorClockEntry);
        values.put(CommuneContract.PollVote.COLUMN_NAME_USER_ID, pollVote.timestamp.userID);

        long newRowId;
        newRowId = db.insert(
                CommuneContract.PollVote.TABLE_NAME,
                null,
                values);

        if (newRowId == -1) Log.e(LOG_TAG, "There was a problem inserting a new pollVote.");

        Log.e(LOG_TAG, "Inserted pollVote " +pollVote.toString());

        return true;
    }

    public boolean insertUser(User newUser) {
        //get our vectorclock
        VectorClock myVectorClock = this.getMyVectorClock();

        Integer myClockEntry, messageClockEntry;
        messageClockEntry = newUser.timestamp.vectorClockEntry;
        myClockEntry = myVectorClock.getClockEntries().get(newUser.timestamp.userID);
        //return false if the user is not in our database
        if(myClockEntry == null){
            return false;
        }

        //return false if the message is not in the right order
        if((myClockEntry.intValue() + 1) != messageClockEntry.intValue()){
            Log.e(LOG_TAG + "/insertUser", "Wrong order for user: " + newUser.toString());
            return false;
        }

        //now we can insert the User

        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CommuneContract.User.COLUMN_NAME_MEMBER_GCM_ID, newUser.getGcmID());
        values.put(CommuneContract.User.COLUMN_NAME_MEMBER_USER_ID, newUser.getUserID());
        values.put(CommuneContract.User.COLUMN_NAME_USERNAME, newUser.getUsername());
        values.put(CommuneContract.User.COLUMN_NAME_CLOCK_ENTRY, newUser.timestamp.vectorClockEntry);
        values.put(CommuneContract.User.COLUMN_NAME_USER_ID, newUser.timestamp.userID);

        long newRowId;
        newRowId = db.insertWithOnConflict(
                CommuneContract.User.TABLE_NAME,
                null,
                values,
                SQLiteDatabase.CONFLICT_REPLACE);

        if (newRowId == -1) Log.e(LOG_TAG, "There was a problem inserting a new user.");

        Log.e(LOG_TAG, "Inserted user " + newUser.getUsername());

        return true;
    }

}
