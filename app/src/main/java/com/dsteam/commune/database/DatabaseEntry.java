package com.dsteam.commune.database;

import com.dsteam.commune.model.Timestamp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Cedric on 12/4/2014.
 */
public abstract class DatabaseEntry {

    protected String type;

    protected Timestamp timestamp;

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public abstract JSONObject toJSON() throws JSONException;

}
