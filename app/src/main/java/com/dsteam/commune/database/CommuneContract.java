package com.dsteam.commune.database;

import android.provider.BaseColumns;

/**
 * Created by jpedro on 11.11.14.
 * Defines table and column names for the Commune sqlite database.
 */
public class CommuneContract {

    // TODO:  http://developer.android.com/training/basics/data-storage/databases.html



    public static abstract class Poll implements BaseColumns {
        public static final String TABLE_NAME = "poll";
        public static final String COLUMN_NAME_POLL_USER_ID = "user_id";
        public static final String COLUMN_NAME_QUESTION = "question";
        public static final String COLUMN_NAME_CLOCK_ENTRY = "clock_entry";

        public static final String TABLE_CREATE = "create table "
                + TABLE_NAME
                + "("
                + COLUMN_NAME_POLL_USER_ID + " text, "
                + COLUMN_NAME_QUESTION + " text,"
                + COLUMN_NAME_CLOCK_ENTRY + " integer, "
                + "primary key("
                + COLUMN_NAME_POLL_USER_ID + ", "
                + COLUMN_NAME_CLOCK_ENTRY
                + "));";
    }

    public static abstract class PollOption implements BaseColumns{
        public static final String TABLE_NAME = "polloption";
        public static final String COLUMN_NAME_POLL_ID = "poll_id";
        public static final String COLUMN_NAME_POLL_USER_ID = "poll_user_id";
        public static final String COLUMN_NAME_OPTION = "answer";
        public static final String COLUMN_NAME_OPTION_ID="option_id";

        public static final String TABLE_CREATE = "create table "
                +TABLE_NAME
                +"("
                +COLUMN_NAME_POLL_USER_ID + " text, "
                +COLUMN_NAME_OPTION +" text, "
                +COLUMN_NAME_POLL_ID +" integer, "
                +COLUMN_NAME_OPTION_ID +" integer, "
                +"primary key("
                +COLUMN_NAME_POLL_USER_ID + ", "
                +COLUMN_NAME_POLL_ID +", "
                +COLUMN_NAME_OPTION_ID
                +"));";
    }



    public static abstract class PollVote implements BaseColumns {
        public static final String TABLE_NAME = "pollvote";
        public static final String COLUMN_NAME_USER_ID = "user_id";
        public static final String COLUMN_NAME_POLL_ID = "poll_id";
        public static final String COLUMN_NAME_OPTION_ID = "option_id";
        public static final String COLUMN_NAME_CLOCK_ENTRY = "clock_entry";
        public static final String COLUMN_NAME_POLL_USER_ID = "poll_user_id";

        public static final String TABLE_CREATE = "create table "
                + TABLE_NAME
                + "("
                + COLUMN_NAME_USER_ID + " text, "
                + COLUMN_NAME_POLL_ID + " integer, "
                + COLUMN_NAME_POLL_USER_ID + " text, "
                + COLUMN_NAME_OPTION_ID + " integer, "
                + COLUMN_NAME_CLOCK_ENTRY + " integer, "
                + "primary key("
                + COLUMN_NAME_CLOCK_ENTRY + ", "
                + COLUMN_NAME_USER_ID
                + "));";
    }

    public static abstract class ToDo implements BaseColumns {
        public static final String TABLE_NAME = "todo";
        public static final String COLUMN_NAME_LISTNAME = "listname";
        public static final String COLUMN_NAME_LISTENTRY = "listentry";
        public static final String COLUMN_NAME_TODO_USER_ID = "todo_user_id";
        public static final String COLUMN_NAME_CLOCK_ENTRY = "clock_entry";
        public static final String COLUMN_NAME_USER_ID = "user_id";

        public static final String TABLE_CREATE = "create table "
                + TABLE_NAME
                + "("
                + COLUMN_NAME_LISTNAME + " text, "
                + COLUMN_NAME_LISTENTRY + " text, "
                + COLUMN_NAME_TODO_USER_ID + " text, "
                + COLUMN_NAME_USER_ID + " text, "
                + COLUMN_NAME_CLOCK_ENTRY + " integer, "
                + "primary key("
                + COLUMN_NAME_CLOCK_ENTRY + ", "
                + COLUMN_NAME_USER_ID
                + "));";
    }

    public static abstract class MyUser implements BaseColumns {
        public static final String TABLE_NAME = "myuser";
        public static final String COLUMN_NAME_CLOCK_ENTRY = "clock_entry";
        public static final String COLUMN_NAME_MY_USER_ID = "my_user_id";
        public static final String COLUMN_NAME_MY_GCM_ID = "my_gcm_id";
        public static final String COLUMN_NAME_USERNAME = "username";

        public static final String TABLE_CREATE = "create table "
                + TABLE_NAME
                + "("
                + COLUMN_NAME_MY_USER_ID + " text, "
                + COLUMN_NAME_CLOCK_ENTRY + " integer, "
                + COLUMN_NAME_MY_GCM_ID + " text, "
                + COLUMN_NAME_USERNAME + " text, "
                + "primary key("
                + COLUMN_NAME_MY_USER_ID
                + "));";
    }

    public static abstract class User implements BaseColumns {
        public static final String TABLE_NAME = "user";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_MEMBER_USER_ID = "member_user_id";
        public static final String COLUMN_NAME_MEMBER_GCM_ID = "member_gcm_id";
        public static final String COLUMN_NAME_USER_ID = "user_id";
        public static final String COLUMN_NAME_CLOCK_ENTRY = "clock_entry";

        public static final String TABLE_CREATE = "create table "
                + TABLE_NAME
                + "("
                + COLUMN_NAME_USER_ID + " integer, "
                + COLUMN_NAME_USERNAME + " text, "
                + COLUMN_NAME_MEMBER_USER_ID + " text, "
                + COLUMN_NAME_MEMBER_GCM_ID + " text, "
                + COLUMN_NAME_CLOCK_ENTRY + " integer, "
                + "primary key("
                + COLUMN_NAME_MEMBER_USER_ID
//                + COLUMN_NAME_USER_ID + ", "
//                + COLUMN_NAME_CLOCK_ENTRY
                + "));";
    }


}
