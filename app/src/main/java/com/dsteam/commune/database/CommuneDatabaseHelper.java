package com.dsteam.commune.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by jpedro on 11.11.14.
 * Helper class to create, update and open databases for storing Commune Data.
 */
public class CommuneDatabaseHelper extends SQLiteOpenHelper {

    // TODO: http://developer.android.com/training/basics/data-storage/databases.html

    public static final String DB_NAME = "commune.sqlite";
    private static final Integer VERSION = 1;

    public CommuneDatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CommuneContract.User.TABLE_CREATE);
        sqLiteDatabase.execSQL(CommuneContract.MyUser.TABLE_CREATE);
        sqLiteDatabase.execSQL(CommuneContract.Poll.TABLE_CREATE);
        sqLiteDatabase.execSQL(CommuneContract.PollOption.TABLE_CREATE);
        sqLiteDatabase.execSQL(CommuneContract.PollVote.TABLE_CREATE);
        sqLiteDatabase.execSQL(CommuneContract.ToDo.TABLE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        //do nothing?
    }
}
