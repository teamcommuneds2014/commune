package com.dsteam.commune.database;

import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Cedric on 12/3/2014.
 */
public class ToDo extends DatabaseEntry{
    String listName;
    String listEntry;
    String toDoUserId;
    String userId;
    int clockEntry;

    @Override
    public JSONObject toJSON() throws JSONException {
        return null;
    }

    public ToDo(String listname, String listentry, String todouserid, String userid, int clockentry){
        this.type = "todo";
        this.listName = listname;
        this.listEntry = listentry;
        this.toDoUserId = todouserid;
        this.userId = userid;
        this.clockEntry = clockentry;
    }

    public ToDo(Cursor cursor){
        this.type = "todo";
        this.listName = cursor.getString(cursor.getColumnIndex(CommuneContract.ToDo.COLUMN_NAME_LISTNAME));
        this.listEntry = cursor.getString(cursor.getColumnIndex(CommuneContract.ToDo.COLUMN_NAME_LISTENTRY));
        this.toDoUserId = cursor.getString(cursor.getColumnIndex(CommuneContract.ToDo.COLUMN_NAME_TODO_USER_ID));
        this.userId = cursor.getString(cursor.getColumnIndex(CommuneContract.ToDo.COLUMN_NAME_USER_ID));
        this.clockEntry = cursor.getInt(cursor.getColumnIndex(CommuneContract.ToDo.COLUMN_NAME_CLOCK_ENTRY));
    }
}
