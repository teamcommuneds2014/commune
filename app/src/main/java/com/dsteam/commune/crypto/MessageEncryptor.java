package com.dsteam.commune.crypto;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.dsteam.commune.protocol.MessageHelper;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by ribin on 27.11.2014.
 * this class encrypts and decrypts messages using the groupID
 */

public class MessageEncryptor {

    private static final String ALGORITHM = "AES";
    private static String groupIDKey = "default";
    private final static String HEX = "0123456789ABCDEF";
    private Cipher cipher;
    private SecretKey key;
    String stringKey;
    SharedPreferences.Editor editor;

    public MessageEncryptor(Context context) throws Exception
    {
        String preferenceFileKey = MessageHelper.PREF_FILE_KEY;
        String prefKeyName = MessageHelper.PREF_KEY_NAME;
        SharedPreferences prefs = context.getSharedPreferences(preferenceFileKey, Context.MODE_PRIVATE);
        if(prefs.contains(prefKeyName))
        {
            //we already have the key in sharedPrefs
            stringKey = prefs.getString(prefKeyName, "");
            //decode the key from string to secretkey object
            byte[] encodedKey = Base64.decode(stringKey, Base64.DEFAULT);
            key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "DES");

        }else{
            //TODO should this be here?
            //we don't have a key, so create a new one and save it in sharedPrefs(only done once per group)
            if(groupIDKey.equals("default")) {

                createNewGroupId();
                key = generateKey();
                //encode key to string to store in sharedPrefs
                stringKey = Base64.encodeToString(key.getEncoded(), Base64.DEFAULT).replace("\n", "");
                editor = prefs.edit();
                editor.putString(prefKeyName, stringKey);
                editor.commit();

            }
        }

        cipher = Cipher.getInstance(ALGORITHM);
    }


    public String encryptMessage(String plainMessage) throws Exception
    {
        cipher.init(Cipher.ENCRYPT_MODE, key);

        return toHex(cipher.doFinal(plainMessage.getBytes()));
    }

    public String  decryptMessage(String encryptedMessage) throws Exception
    {
        cipher.init(Cipher.DECRYPT_MODE, key);

        return new String(cipher.doFinal(toByte(encryptedMessage)));
    }

    public SecretKey generateKey () throws Exception
    {

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(groupIDKey.toCharArray(), groupIDKey.getBytes(), 128, 256);
        SecretKey tmp = factory.generateSecret(spec);
        return new SecretKeySpec(tmp.getEncoded(), ALGORITHM);
    }

    // Helper methods

    private byte[] toByte(String hexString)
    {
        int len = hexString.length()/2;

        byte[] result = new byte[len];

        for (int i = 0; i < len; i++)
            result[i] = Integer.valueOf(hexString.substring(2*i, 2*i+2), 16).byteValue();
        return result;
    }

    public String toHex(byte[] stringBytes)
    {
        StringBuffer result = new StringBuffer(2*stringBytes.length);

        for (byte stringByte : stringBytes) {
            result.append(HEX.charAt((stringByte >> 4) & 0x0f)).append(HEX.charAt(stringByte & 0x0f));
        }

        return result.toString();
    }

    //create a unique new groupID(this method is only called once
    public void createNewGroupId()
    {
        SecureRandom random = new SecureRandom();
        groupIDKey =  new BigInteger(130, random).toString(32);
    }

}
