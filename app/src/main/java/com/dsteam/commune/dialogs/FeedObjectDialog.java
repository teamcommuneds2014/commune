package com.dsteam.commune.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.dsteam.commune.R;
import com.dsteam.commune.activities.NewPollActivity;

/**
 * Created by march_000 on 15.12.2014.
 */
public class FeedObjectDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_choose_type_of_fobject)
                .setItems(R.array.feed_object_types_array, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            Intent intent = new Intent(getActivity(), NewPollActivity.class);
                            startActivity(intent);
                        }
                    }
                });
        return builder.create();
    }
}
