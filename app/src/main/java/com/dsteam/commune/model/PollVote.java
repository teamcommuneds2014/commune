package com.dsteam.commune.model;

import android.database.Cursor;

import com.dsteam.commune.database.CommuneContract;
import com.dsteam.commune.database.DatabaseEntry;
import com.dsteam.commune.protocol.MessageHelper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jpedro on 11.12.14.
 */
public class PollVote extends DatabaseEntry {

// ATTRIBUTES

    private String type = "pollvote";

    private String voterID;

    private final int pollID;

    private final String pollUserID;

    private final int optionID;

// CONSTRUCTOR

    // Constructor used when user votes through UI (voterID and timestamp added by DB)
    public PollVote(int somePollID, String somePollUserID, int someOptionID) {
        pollID = somePollID;
        pollUserID = somePollUserID;
        optionID = someOptionID;
    }

    public PollVote(String someVoterID, int somePollID, String somePollUserID, int someOptionID, int someClockEntry){
        voterID = someVoterID;
        pollID = somePollID;
        pollUserID = somePollUserID;
        optionID = someOptionID;
        timestamp = new Timestamp(voterID, someClockEntry);
    }

    public PollVote(Cursor c){
        String userid = c.getString(c.getColumnIndex(CommuneContract.PollVote.COLUMN_NAME_USER_ID));
        int clockentry = c.getInt(c.getColumnIndex(CommuneContract.PollVote.COLUMN_NAME_CLOCK_ENTRY));
        voterID = userid;
        pollID = c.getInt(c.getColumnIndex(CommuneContract.PollVote.COLUMN_NAME_POLL_ID));
        pollUserID = c.getString(c.getColumnIndex(CommuneContract.PollVote.COLUMN_NAME_POLL_USER_ID));
        optionID = c.getInt(c.getColumnIndex(CommuneContract.PollVote.COLUMN_NAME_OPTION_ID));
        timestamp = new Timestamp(userid, clockentry);
    }

// GETTERS AND SETTERS

    public void setVoterID(String voterID) {
        this.voterID = voterID;
    }

    public void setTimestamp(Timestamp someTimestamp) {
        this.timestamp = someTimestamp;
    }

    public int getOptionID() {
        return optionID;
    }

    public String getPollUserID() {
        return pollUserID;
    }

    public int getPollID() {
        return pollID;
    }

    public String getVoterID() {
        return voterID;
    }

    public String getType() {
        return type;
    }

// FORMAT METHOD

    //   vote: { voter_id: 123, poll_id: 2, poll_user: 123, option_id: 1, timestamp: { user_id: 123, clock: 3 } }
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put(MessageHelper.PollVote.VOTER_ID_KEY, voterID);
        jsonObject.put(MessageHelper.PollVote.POLL_ID_KEY, pollID);
        jsonObject.put(MessageHelper.PollVote.POLL_USER_KEY, pollUserID);
        jsonObject.put(MessageHelper.PollVote.OPTION_ID_KEY, optionID);
        jsonObject.put(MessageHelper.Timestamp.TIMESTAMP_LABEL, timestamp.toJSON());

        return jsonObject;
    }

    public static PollVote fromJSON(JSONObject voteJSON) throws JSONException {
        String someVoterID = voteJSON.getString(MessageHelper.PollVote.VOTER_ID_KEY);
        String somePollUser = voteJSON.getString(MessageHelper.PollVote.POLL_USER_KEY);
        int somePollID = voteJSON.getInt(MessageHelper.PollVote.POLL_ID_KEY);
        int someOptionID = voteJSON.getInt(MessageHelper.PollVote.OPTION_ID_KEY);
        Timestamp someTimestamp = Timestamp.fromJSON(voteJSON.getJSONObject(MessageHelper.Timestamp.TIMESTAMP_LABEL));

        return new PollVote(someVoterID, somePollID, somePollUser, someOptionID, someTimestamp.vectorClockEntry);
    }

    public String toString() {
        return "PollVote[" + voterID + "; " + pollID + "; " + pollUserID + "; " + optionID + "]";
    }

}
