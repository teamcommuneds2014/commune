package com.dsteam.commune.model;

import android.database.Cursor;

import com.dsteam.commune.database.CommuneContract;
import com.dsteam.commune.database.DatabaseEntry;
import com.dsteam.commune.protocol.MessageHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpedro on 04.12.14.
 */
public class PollOption extends DatabaseEntry {

// ATTRIBUTES

    private String option;

    private List<User> voters;

// DB ATTRIBUTES

    private int optionID;

    private int pollID;

    private String pollUserID;

// CONSTRUCTOR

    // TODO: OPTIONID?
    public PollOption(String opt, int somePollID, int someOptionID, String somePollUser, List<User> someVoters) {
        option = opt;
        if (someVoters == null) voters = new ArrayList<User>();
        else voters = someVoters;
        pollID = somePollID;
        pollUserID = somePollUser;
        optionID = someOptionID;
    }

    public PollOption(String opt, int somePollID, int someOptionID, String somePollUser) {
        option = opt;
        voters = new ArrayList<User>();
        pollID = somePollID;
        pollUserID = somePollUser;
        optionID = someOptionID;
    }

    public PollOption(Cursor c){
        pollID=c.getInt(c.getColumnIndex(CommuneContract.PollOption.COLUMN_NAME_POLL_ID));
        pollUserID=c.getString(c.getColumnIndex(CommuneContract.PollOption.COLUMN_NAME_POLL_USER_ID));
        option=c.getString(c.getColumnIndex(CommuneContract.PollOption.COLUMN_NAME_OPTION));
        optionID=c.getInt(c.getColumnIndex(CommuneContract.PollOption.COLUMN_NAME_OPTION_ID));
        voters = new ArrayList<User>();
    }

// GETTERS

    public int getOptionID(){
        return optionID;
    }

    public int getPollID(){
        return pollID;
    }

    public String getPollUserID(){
        return pollUserID;
    }

    public String getOption() {
        return option;
    }

    public int getNumberOfVotes() {
        return voters.size();
    }

    public void setVoters(List<User> someVoters) {
        voters = someVoters;
    }

    public PollVote vote() {
        return new PollVote(pollID, pollUserID, optionID);
    }

    public List<User> getVoters(){
        if(voters == null) return new ArrayList<User>();
        return voters;
    }

    public boolean hasVoted(User someUser) {
        if (voters == null) return false;
        for(User voter : voters) {
            if (someUser.equals(voter)) return true;
        }
        return false;
    }

// FORMAT METHODS

    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put(MessageHelper.PollOption.OPTION_KEY, option);
        jsonObject.put(MessageHelper.PollOption.POLL_ID_KEY, pollID);
        jsonObject.put(MessageHelper.PollOption.OPTION_ID_KEY, optionID);
        jsonObject.put(MessageHelper.PollOption.POLL_USER_KEY, pollUserID);

        return jsonObject;
    }

    public static PollOption fromJSON(JSONObject optJSON) throws JSONException {
        String someOption = optJSON.getString(MessageHelper.PollOption.OPTION_KEY);
        int somePollID = optJSON.getInt(MessageHelper.PollOption.POLL_ID_KEY);
        String somePollUserID = optJSON.getString(MessageHelper.PollOption.POLL_USER_KEY);
        int somePollOptionID = optJSON.getInt(MessageHelper.PollOption.OPTION_ID_KEY);
        return new PollOption(someOption, somePollID, somePollOptionID, somePollUserID);
    }

}
