package com.dsteam.commune.model;

import android.database.Cursor;

import com.dsteam.commune.database.CommuneContract;
import com.dsteam.commune.database.DatabaseEntry;
import com.dsteam.commune.protocol.MessageHelper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jpedro on 11.11.14.
 */
public class User extends DatabaseEntry {

// ATTRIBUTES

    // Google Cloud Messaging ID of the Peer
    private String gcmID;

    private String username;

    private String userID;

// CONSTRUCTORS

    public User(String someGcmID, String someName, String someID, Timestamp someTimestamp) {
        type = "user";
        gcmID = someGcmID;
        userID = someID;
        username = someName;
        timestamp = someTimestamp;
    }

    public User(Cursor c) {
        type = "user";
        userID = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_MEMBER_USER_ID));
        gcmID = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_MEMBER_GCM_ID));
        username = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_USERNAME));
        String userid = c.getString(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_USER_ID));
        int clockentry = c.getInt(c.getColumnIndex(CommuneContract.User.COLUMN_NAME_CLOCK_ENTRY));
        timestamp = new Timestamp(userid,clockentry);
    }

// GETTERS

    public String getGcmID() {
        return gcmID;
    }

    public String getUsername() {
        return username;
    }

    public String getUserID() {
        return userID;
    }

// FORMAT METHODS

    // user: { user_id: 124, gcm_id: ... , username: "marc", timestamp: { user_id: 123, clock: 2 } } }
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put(MessageHelper.User.USER_ID_KEY, userID);
        jsonObject.put(MessageHelper.User.GCM_ID_KEY, gcmID);
        jsonObject.put(MessageHelper.User.USERNAME_KEY, username);
        jsonObject.put(MessageHelper.Timestamp.TIMESTAMP_LABEL, timestamp.toJSON());

        return jsonObject;
    }

    public static User fromJSON(JSONObject userJSON) throws JSONException {
        String someUserID = userJSON.getString(MessageHelper.User.USER_ID_KEY);
        String someGCMID = userJSON.getString(MessageHelper.User.GCM_ID_KEY);
        String someName = userJSON.getString(MessageHelper.User.USERNAME_KEY);
        Timestamp someTimestamp = Timestamp.fromJSON(userJSON.getJSONObject(MessageHelper.Timestamp.TIMESTAMP_LABEL));

        return new User(someGCMID, someName, someUserID, someTimestamp);
    }


    @Override
    public boolean equals(Object o) {
        if(!(o instanceof User)) return false;
        else if (((User) o).getUserID().equals(this.getUserID())) return true;
        return false;
    }

    @Override
    public String toString() {
        String userStr;
        try {
            userStr = toJSON().toString();
        } catch (JSONException e) {
            e.printStackTrace();
            userStr = "User[" + username + "; " + userID + "; " + gcmID + "]";
        }
        return userStr;
    }
}
