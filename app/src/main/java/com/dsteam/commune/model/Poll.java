package com.dsteam.commune.model;

import android.database.Cursor;

import com.dsteam.commune.database.CommuneContract;
import com.dsteam.commune.database.DatabaseEntry;
import com.dsteam.commune.protocol.MessageHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpedro on 11.11.14.
 */
public class Poll extends DatabaseEntry implements FeedObject {

// CONSTANTS
    // TODO: NEEDED?
    private int MAX_NUMBER_OF_OPTIONS = 10;

// ATTRIBUTES

    private int optionCount = 0;

    private String question;

    private List<PollOption> options;

// DB ATTRIBUTES

    private int pollID;

    private String pollUserID;

    private final String type = "poll";

// GETTERS

    public String getQuestion(){return question;}

    public List<PollOption> getOptions(){
        return options;
    }

    public int getPollID(){
        return pollID;
    }

    public String getPollUserID(){
        return pollUserID;
    }

// CONSTRUCTORS

    public Poll(String someQuestion, List<PollOption> someOptions, String somePollUserID, int someClockEntry) {
        question = someQuestion;
        timestamp = new Timestamp(somePollUserID, someClockEntry);
        pollID = someClockEntry;
        pollUserID = somePollUserID;
        if (someOptions == null) options = new ArrayList<PollOption>();
        else options = someOptions;
    }

    public Poll(String someQuestion, String somePollUserID, int someClockEntry) {
        question = someQuestion;
        timestamp = new Timestamp(somePollUserID, someClockEntry);
        pollID = someClockEntry;
        pollUserID = somePollUserID;
        options = new ArrayList<PollOption>();
    }

    public Poll(Cursor c, Cursor pollOptionsCursor){
        int clockentry = c.getInt(c.getColumnIndex(CommuneContract.Poll.COLUMN_NAME_CLOCK_ENTRY));
        pollUserID = c.getString(c.getColumnIndex(CommuneContract.Poll.COLUMN_NAME_POLL_USER_ID));
        pollID = clockentry;
        question = c.getString(c.getColumnIndex(CommuneContract.Poll.COLUMN_NAME_QUESTION));
        timestamp = new Timestamp(pollUserID, clockentry);
        options = new ArrayList<PollOption>();
        PollOption pollOption;
        for(int i=0;i < pollOptionsCursor.getCount();i++){
            pollOption = new PollOption(pollOptionsCursor);
            options.add(pollOption);
            pollOptionsCursor.moveToNext();
        }
    }

// METHODS

    public void addOption(String opt) {
        options.add(new PollOption(opt, pollID, optionCount++, pollUserID));
    }

    public void addOption(PollOption opt) {
        options.add(opt);
    }

    public int getNumberOfOptions() {
        return options.size();
    }

    public boolean hasVoted(User someUser) {
        if(options == null || options.isEmpty()) return false;
        boolean hasVoted = false;
        for (PollOption option : options) {
            hasVoted = hasVoted || option.hasVoted(someUser);
        }
        return hasVoted;
    }

// FORMAT METHODS

    //   poll: { question: "lunch?", poll_id: 2, poll_user: 123, timestamp: { user_id: 123, clock: 78 },
    //           options: [{ option: "no", option_id: 1 }, { option: "yes", ... }] } }
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        JSONArray optionsJson = new JSONArray();

        jsonObject.put(MessageHelper.Poll.QUESTION_KEY, question);

        for(PollOption opt : options) {
            optionsJson.put(opt.toJSON());
        }

        jsonObject.put(MessageHelper.Poll.OPTIONS_KEY, optionsJson);
        jsonObject.put(MessageHelper.Poll.POLL_ID_KEY, pollID);
        jsonObject.put(MessageHelper.Poll.POLL_USER_KEY, pollUserID);
        jsonObject.put(MessageHelper.Timestamp.TIMESTAMP_LABEL, timestamp.toJSON());

        return jsonObject;
    }

    public static Poll fromJSON(JSONObject pollJSON) throws JSONException {
        String someQuestion = pollJSON.getString(MessageHelper.Poll.QUESTION_KEY);
        String somePollID = pollJSON.getString(MessageHelper.Poll.POLL_ID_KEY);
        String somePollUserID = pollJSON.getString(MessageHelper.Poll.POLL_USER_KEY);
        Timestamp someTimestamp = Timestamp.fromJSON(pollJSON.getJSONObject(MessageHelper.Timestamp.TIMESTAMP_LABEL));

        List<PollOption> someOptions = new ArrayList<PollOption>();

        JSONArray opts = pollJSON.getJSONArray(MessageHelper.Poll.OPTIONS_KEY);

        for(int i = 0; i < opts.length(); i++) {
            someOptions.add(PollOption.fromJSON(opts.getJSONObject(i)));
        }

        return new Poll(someQuestion, someOptions, somePollUserID, someTimestamp.vectorClockEntry);
    }


    @Override
    public String toString() {
        String poll = "Poll[" + pollID + "; " + pollUserID + "; " + question + "]";
        for(PollOption option : options) {
            poll = poll + "\n" + option.getVoters().toString();
        }

        return poll;
    }
}