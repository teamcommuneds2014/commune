package com.dsteam.commune.model;

import com.dsteam.commune.protocol.MessageHelper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jpedro on 06.12.14.
 */
public class Timestamp {

    public final String userID;
    public final int vectorClockEntry;

    public Timestamp(String someUserID, int someClock) {
        userID = someUserID;
        vectorClockEntry = someClock;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();

        json.put(MessageHelper.Timestamp.USER_ID_KEY, userID);
        json.put(MessageHelper.Timestamp.CLOCK_KEY, vectorClockEntry);

        return json;
    }

    public static Timestamp fromJSON(JSONObject timestampJSON) throws JSONException {
        return new Timestamp(timestampJSON.getString(MessageHelper.Timestamp.USER_ID_KEY),
                             timestampJSON.getInt(MessageHelper.Timestamp.CLOCK_KEY));
    }

}
