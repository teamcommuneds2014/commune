package com.dsteam.commune.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by jpedro on 06.12.14.
 */
// TODO: COPY FROM CHAT PROJECT?
public class VectorClock {

    private HashMap<String, Integer> clockEntries;

    public VectorClock(HashMap<String, Integer> someClockEntries) {
        clockEntries = someClockEntries;
    }

    public HashMap<String,Integer> getClockEntries(){
        return clockEntries;
    }

// TODO: MORE METHODS?

    // returns true, iff otherClock has (more than one entry greater than this or otherClock has
    //               one entry which is min. 2 greater than this) and (otherClock has all entries greater equal this).
    public boolean hasNegativeGap(VectorClock otherClock) {
        HashMap<String, Integer> otherClockEntries = otherClock.getClockEntries();
        Set<String> keySet = new HashSet<String>(otherClockEntries.keySet());
        keySet.addAll(new HashSet<String>(this.clockEntries.keySet()));

        int diff = 0;

        for (String k : keySet) {
            Integer otherValue = otherClockEntries.get(k);
            if (otherValue == null) otherValue = 0;
            Integer thisValue = this.clockEntries.get(k);
            if (thisValue == null) thisValue = 0;

            if (thisValue < otherValue) diff = diff + (otherValue - thisValue);
        }

        return diff > 1;
    }

// FORMAT METHODS

    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();

        if (clockEntries != null && !clockEntries.isEmpty()) {
            Set<String> keys = clockEntries.keySet();

            for (String k : keys) {
                jsonObject.put(k, clockEntries.get(k));
            }
        }

        return jsonObject;
    }

    public static VectorClock fromJSON(JSONObject clockJSON) throws JSONException {
        Iterator<String> userKeys = clockJSON.keys();
        HashMap<String, Integer> someClockEntries = new HashMap<String, Integer>();

        while(userKeys.hasNext()) {
            String key = userKeys.next();
            someClockEntries.put(key, clockJSON.getInt(key));
        }

        return new VectorClock(someClockEntries);
    }

}
