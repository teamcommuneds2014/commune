package com.dsteam.commune.communication;

import android.os.AsyncTask;
import android.util.Log;

import com.dsteam.commune.communication.model.Message;
import com.dsteam.commune.communication.peer.IP2PComListener;
import com.dsteam.commune.communication.peer.P2PComException;
import com.dsteam.commune.communication.peer.P2PCommunication;
import com.dsteam.commune.crypto.MessageEncryptor;

/**
 * Created by jpedro on 08.12.14.
 */
public class SenderTask extends AsyncTask<Message, Void, Boolean> {

    private static final String LOG_TAG = SenderTask.class.getSimpleName();

    private MessageEncryptor mCrypto;
    private IP2PComListener mMessageListener;

    public SenderTask(IP2PComListener listener, MessageEncryptor someCrypto) {
        mCrypto = someCrypto;
        mMessageListener = listener;
    }

    @Override
    protected Boolean doInBackground(Message... messages) {
        if(messages == null || messages.length == 0) {
            Log.e(LOG_TAG, "No Message was passed as parameter");
            return false;
        }

        Message message = messages[0];

        if (message.getDestination() == null) {
            Log.e(LOG_TAG + "/doInBackground", "Destination was null");
            return false;
        } else {
            Log.d(LOG_TAG + "/doInBackground", "Destination: " + message.getDestination().toString());
        }

        boolean successfulSend = false;
        int tries = 0;

        while (!successfulSend) {
            if (message == null ) {
                Log.e(LOG_TAG, "No peer was available.");
                return false;
            }
            tries++;
            successfulSend = tryToSendMessage(message);
            if (!successfulSend) {
                Log.e(LOG_TAG, "Sending did not succeed. Attempt: " + tries + " Message: "+ message.toJSON().toString());
                // retry with same destination
                successfulSend = tryToSendMessage(message);
                tries++;
                if (!successfulSend) {
                    Log.e(LOG_TAG, "Sending did not succeed. Attempts: " + tries + " Message: "+ message.toJSON().toString());
                } else {
                    Log.d(LOG_TAG, "Message successfully sent after " + tries + " attempt(s)! Message: " + message.toJSON().toString());
                }

                // get new broadcastMessage with different destination
                // returns null, if no destination left to choose
                message = message.tryNext();
            } else {
                Log.d(LOG_TAG, "Message successfully sent after " + tries + " attempt(s)! Message: " + message.toJSON().toString());
            }
        }
        return true;
    }

    private boolean tryToSendMessage(Message msg) {
        P2PCommunication p2PComm = new P2PCommunication(0);
        p2PComm.addReceiveP2PListener(mMessageListener);
        try {
            p2PComm.connect(msg.getDestination());
            return p2PComm.sendMessage(mCrypto.encryptMessage(msg.toJSON().toString()));
        } catch (P2PComException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "It was not possible to connect/send.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Log.e(LOG_TAG + "/tryToSendMessage", "Finally terminating connection");
            p2PComm.terminateConnection();
        }
        return false;
    }

}
