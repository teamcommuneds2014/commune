package com.dsteam.commune.communication.peer;

/**
 * Created by lukas on 22.11.14.
 */
public interface IP2PComListener {

    public void onConnectionEstablished(P2PCommunication connection);

    public void onMessageReceived(String msg, P2PCommunication connection);

    public void onConnectionLost(P2PCommunication connection);

    public void onConnectionTermination();

}
