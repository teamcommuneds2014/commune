package com.dsteam.commune.communication;

import com.dsteam.commune.communication.model.BroadcastMessage;
import com.dsteam.commune.model.User;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by jpedro on 12.11.14.
 *
 * Interface for class responsible for the whole communication in Commune's distributed system.
 * Coordinates the requests to/from Server, Peer and GCM.
 *
 */
public interface IDistributedComm {

    // Chooses and send to log(n) Peers (from all Users) a broadcast message, specifying for each destination which peers
    // should be reached through them.
    public boolean broadcast(JSONObject message);

    // Chooses and send to log(n) Peers (from broadcastPeers) a broadcast message, specifying for each destination which peers
    // should be reached through them.
    public boolean broadcast(JSONObject message, List<User> broadcastPeers);

    public void forward(BroadcastMessage broadcastMessage);

}
