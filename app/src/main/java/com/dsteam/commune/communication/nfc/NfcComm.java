package com.dsteam.commune.communication.nfc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;

/**
 * Created by ribin on 14.12.2014.
 */
public class NfcComm implements INfcComm {

    private boolean hasAndroidBeam;

    //our current context
    private Context context;

    private NfcAdapter nfcAdapter;

    // List of URIs to provide to Android Beam
    private Uri[] fileUris;

    // A File object containing the path to the transferred files
    private String filePath;

    public NfcComm(Context newContext)
    {
        context = newContext;
    }

    //checks whether device supports NFC and Android Beam
    public boolean isNfcCompatible()
    {
        // NFC isn't available on the device
        hasAndroidBeam = !(!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC) || (Build.VERSION.SDK_INT <
                Build.VERSION_CODES.JELLY_BEAN_MR1));

        return hasAndroidBeam;
    }

    public void createMessage(String transferFileNames[]) {

        int numOfFiles = transferFileNames.length;
        fileUris = new Uri[numOfFiles];
        File root = android.os.Environment.getExternalStorageDirectory();

        String extDir = root.getAbsolutePath() + "/download/";

        for(int i = 0; i < numOfFiles; i++ ) {


            File requestFile = new File(extDir + transferFileNames[i]);
            //file can only be send, if it is world readable
            requestFile.setReadable(true, false);
            // Get a URI for the File and add it to the list of URIs
            Uri fileUri = Uri.fromFile(requestFile);
            fileUris[i] = fileUri;

            //requestFile.delete();

        }

    }

    public void createMessage(String transferFileName) {

        //fileUris = new Uri[1];
        File root = android.os.Environment.getExternalStorageDirectory();

        String extDir = root.getAbsolutePath() + "/download/";

        File requestFile = new File(extDir + transferFileName);
        //file can only be send, if it is world readable
        requestFile.setReadable(true, false);
        // Get a URI for the File and add it to the list of URIs
        Uri fileUri = Uri.fromFile(requestFile);
        fileUris = new Uri[]{fileUri};

       // requestFile.delete();

    }

    @Override
    public void sendInvitation(Activity activity, String transferFileNames[]) {


        if(hasAndroidBeam) {

            createMessage(transferFileNames);

            nfcAdapter = NfcAdapter.getDefaultAdapter(context);

            FileUriCallback fileUriCallback = new FileUriCallback();
            //set callback method which send uri array to the receiver
            nfcAdapter.setBeamPushUrisCallback(fileUriCallback,activity);

        }

    }

    public void sendUserData(Activity a, String transferFileName)
    {
        createMessage(transferFileName);

        nfcAdapter = NfcAdapter.getDefaultAdapter(context);

        FileUriCallback fileUriCallback = new FileUriCallback();
        //set callback method which send uri array to the receiver
        nfcAdapter.setBeamPushUrisCallback(fileUriCallback, a);
    }

    @Override
    public void accept(Intent incomingIntent) {

        String action = incomingIntent.getAction();
        /*
         * For ACTION_VIEW, the Activity is being asked to display data.
         * Get the URI.
         */
        if (TextUtils.equals(action, Intent.ACTION_VIEW)) {
            // Get the URI from the Intent
            Uri beamUri = incomingIntent.getData();

            if (TextUtils.equals(beamUri.getScheme(), "file")) {
                handleFileUri(beamUri);
            }
        }

    }

    //return the directory path where all the received files are stored
    public void handleFileUri(Uri beamUri) {

        // Get the path part of the URI
        String fileName = beamUri.getPath();
        // Create a File object for this filename(only a reference to the file)
        File copiedFile = new File(fileName);
        // Get a string containing the file's parent directory
        filePath = copiedFile.getParent();


        /*Log.d("Files", "Path: " + filePath);
        File f = new File(filePath);
        File file[] = f.listFiles();
        Log.d("Files", "Size: "+ file.length);
        for (int i=0; i < file.length; i++)
        {
            Log.d("Files", "FileName:" + file[i].getName());
            file[i].delete();
        }


        copiedFile.delete();*/
    }


    /**
     * Callback that Android Beam file transfer calls to get
     * files to share
     */
    private class FileUriCallback implements
            NfcAdapter.CreateBeamUrisCallback {
        public FileUriCallback() {
        }

        /**
         * this method send all files defines in fileUris to the receiver
         */
        @Override
        public Uri[] createBeamUris(NfcEvent event) {
            return fileUris;
        }

    }

    //this method returns the filePath of the files we received
    public String getFilePath()
    {
        if(filePath != null)
            return filePath;
        else
            return null;
    }
}
