package com.dsteam.commune.communication.peer;

import com.dsteam.commune.model.User;

import java.net.InetAddress;

/**
 * Created by lukas on 03.12.14.
 */
public interface IP2PCommunication {

    void connect(InetAddress destIP, int destPort, String queryID) throws P2PComException;

    void connect(User peer) throws P2PComException;

    void terminateConnection();

    boolean sendMessage(String msg);

    void addReceiveP2PListener(IP2PComListener listener);

    void removeReceiveP2PListener(IP2PComListener listener);
}
