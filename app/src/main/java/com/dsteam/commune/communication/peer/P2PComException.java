package com.dsteam.commune.communication.peer;

import java.io.IOException;

/**
 * Created by lukas on 23.11.14.
 */
public class P2PComException extends IOException {

    public P2PComException(String text) {
        super(text);
    }

}
