package com.dsteam.commune.communication.peer;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.dsteam.commune.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by lukas on 21.11.14.
 * Peer to Peer Communication
 * Handles the connection protocol with the server and the other peer
 * You can send msg to the other PEER with the sendMessage method and establish a connection with connect and the corresponding gcm id
 */
public class P2PCommunication implements IP2PCommunication {

    //Timeouts
    private static final int SOCKET_TIMEOUT_RECEIVER = 5000;
    private static final int SOCKET_TIMEOUT_CONNECTING = 2000;
    private static final int SERVER_RESPONSE_TIMEOUT = 5000;
    private static final int CONNECT_RESPONSE_TIMEOUT = 3000;
    private static final int CONNECTION_LOST_ROUNDS = 4;

    private static final int bufferSize = 65535;

    private static final String RECEIVE_MSG_TAG = "MSG_RECEIVED";
    private static final String CONNECTION_LOST_TAG = "CONNECTION_LOST";
    private static final String CONNECTION_ESTABLISHED_TAG = "CONNECTION_EST";
    private static final String CHECK_CONNECTION_TAG = "CHECK_CON";

    private static final String TAG = "P2PCom";

    /**
     * UDP Socket
     */
    private DatagramSocket socket;

    //Threads
    private ReceiveHandler receiveHandler;
    private Receiver receiver;

    /**
     * Unique ID for the Connection
     */
    private String queryID;

    // other peer address
    private volatile InetAddress destIP;
    private volatile int destPort;

    // server address
    private InetAddress serverIP;
    private int serverPort;

    /**
     * State of the connection
     */
    private volatile ConnectionState state;

    /**
     * States if this communication runs over the server
     */
    private volatile boolean comOverServer = false;

    /**
     * State if this communication is in the active part
     */
    private volatile boolean isAsker = true;

    private enum ConnectionState {
        CONNECTED,
        DISCONNECTED,
        RECONNECT
    }

    /**
     *
     * @param port the port on which the communication runs, if 0 -> chose free port randomly
     */
    public P2PCommunication(int port) {
        initServerAddress();
        initSocket(port);
        this.state = ConnectionState.DISCONNECTED;
        receiveHandler = new ReceiveHandler(this);
        receiveHandler.start();
    }

    /**
     * Called by GCMIntendService on ip msg received
     * @param destIP
     * @param destPort
     * @param queryID ID P2P connection
     * @throws P2PComException
     */
    @Override
    public void connect(InetAddress destIP, int destPort, String queryID) throws P2PComException {
        this.isAsker = false;
        this.state = ConnectionState.RECONNECT;
        this.destIP = destIP;
        this.destPort = destPort;
        this.queryID = queryID;
        connectAnswerPeerOverServer(queryID);
    }

    /**
     * Should be called in a separate thread may take a few seconds
     * @param user The user that you want to talk to
     * @throws P2PComException if something fails -> timeouts,other
     */
    @Override
    public void connect(User user) throws P2PComException {
        this.isAsker = true;
        this.state = ConnectionState.RECONNECT;
        this.queryID= P2PMessagesUtils.getNewQueryKey();
        connectAskPeerOverServer(user.getGcmID());
    }

    /**
     * Should be called when the communication is not anymore needed
     */
    @Override
    public synchronized void terminateConnection() {
        Log.i(TAG,"terminate Connection");
        if(receiver!=null)
            receiver.StopReceive();
        receiveHandler.stopHandlerLoop();
        if(!socket.isClosed())
            socket.close();
    }

    /**
     * States if the communication runs over the Server or p2p
     * @return
     */
    public boolean isCommunicatingOverServer() {
        return this.comOverServer;
    }

    private void startUDPCommunicationThreads() {
        receiver = new Receiver(socket);
        receiver.start();
    }

    private void initServerAddress() {
        try {
            serverIP = InetAddress.getByName(ServerConfig.SERVER_IP);
            serverPort = ServerConfig.SERVER_PORT;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    private void connectAskPeerOverServer(String gcmName) throws P2PComException {
        String askIP = P2PMessagesUtils.getToServerAskIPMessage(gcmName, this.queryID);
        comOverServer = false;

        sendMsgToServer(askIP);

        JSONObject respond;
        String respondType;
        String clientIP;
        String queryIDResp;
        int clientPort;
        try {
            do {
                respond = waitForMessage(SERVER_RESPONSE_TIMEOUT);
                respondType = respond.getString(P2PMessagesUtils.TYPE_KEY);
            } while((respondType.equals(P2PMessagesUtils.TYPE_PUNCH_HOLE)));

            clientIP = respond.getString(P2PMessagesUtils.CLIENT_IP_KEY);
            clientPort = Integer.parseInt(respond.getString(P2PMessagesUtils.CLIENT_PORT_KEY));
            queryIDResp = respond.getString(P2PMessagesUtils.MSG_ID_KEY);


            boolean isNull;
            isNull = respondType==null || clientIP == null || queryIDResp==null;

            if(isNull || !respondType.equals(P2PMessagesUtils.TYPE_CLIENT_ADDR) || !queryIDResp.equals(queryID))
                throw new P2PComException("Connection failed: false ServerRespond");

            destIP = InetAddress.getByName(clientIP);
            destPort = clientPort;

        } catch (UnknownHostException e) {
            throw new P2PComException("Connection failed: invalid client IP");
        } catch (IOException e) {
            throw new P2PComException("Connection failed: no ServerRespond");
        } catch (JSONException e) {
            throw new P2PComException("Connection failed: false ServerRespond");
        }

        int doOnServer = 2;
        do {
            String connectMsg = P2PMessagesUtils.getAskConnectMessage(queryID);
            sendMessage(connectMsg);

            JSONObject peerRespond;
            String peerRespondType;
            String queryIDpeerResp;
            try {
                doOnServer--;
                peerRespond = waitForMessage(CONNECT_RESPONSE_TIMEOUT);

                peerRespondType = peerRespond.getString(P2PMessagesUtils.TYPE_KEY);
                queryIDpeerResp = peerRespond.getString(P2PMessagesUtils.MSG_ID_KEY);

                boolean isNull;
                isNull = peerRespondType == null || queryIDpeerResp == null;

                if (isNull || !peerRespondType.equals(P2PMessagesUtils.TYPE_CONNECT_OK) || !queryIDpeerResp.equals(queryID))
                    throw new P2PComException("Connection failed: false PeerRespond");

            } catch (IOException e) {
                if(doOnServer==1) {
                    this.comOverServer = true;
                }
                else
                    throw new P2PComException("Connection failed: no PeerRespond");
            } catch (JSONException e) {
                throw new P2PComException("Connection failed: false PeerRespond");
            }
        } while(doOnServer==1 && this.comOverServer == true);

        connectSuccess();
    }

    private void connectAnswerPeerOverServer(String queryID) throws P2PComException {
        comOverServer = false;
        punchHole();

        String msgServer = P2PMessagesUtils.getClientIPMessage(queryID);
        sendMsgToServer(msgServer);

        punchHole();
        punchHole();

        int tryOnServer = 2;

        do {
            JSONObject peerRespond;
            String peerRespondType;
            String queryIDpeerResp;
            try {
                tryOnServer--;
                peerRespond = waitForMessage(CONNECT_RESPONSE_TIMEOUT);

                peerRespondType = peerRespond.getString(P2PMessagesUtils.TYPE_KEY);
                queryIDpeerResp = peerRespond.getString(P2PMessagesUtils.MSG_ID_KEY);

                boolean isNull;
                isNull = peerRespondType == null || queryIDpeerResp == null;

                if (isNull || !peerRespondType.equals(P2PMessagesUtils.TYPE_CONNECT) || !queryIDpeerResp.equals(queryID))
                    throw new P2PComException("Connection failed: false PeerRespond");


            } catch (IOException e) {
                if(tryOnServer==1) {
                    this.comOverServer = true;
                }
                else
                    throw new P2PComException("Connection failed: no PeerRespond");
            } catch (JSONException e) {
                throw new P2PComException("Connection failed: false PeerRespond");
            }
        } while(tryOnServer==1 &&  this.comOverServer);

        String msgPeer = P2PMessagesUtils.getConnectOKMessage(queryID);
        sendMessage(msgPeer);

        connectSuccess();

    }

    private void connectSuccess() {
        this.state = ConnectionState.CONNECTED;
        startUDPCommunicationThreads();
        sendMsgToHandler(CONNECTION_ESTABLISHED_TAG, "");
    }

    private void punchHole() {
        String msg = P2PMessagesUtils.getPunchHoleMessage();
        sendMessage(msg);
    }

    private void initSocket(int port) {
        try {
            this.socket = new DatagramSocket(port);
            socket.setSoTimeout(SOCKET_TIMEOUT_CONNECTING);
        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private JSONObject waitForMessage(int timeout) throws IOException {
        byte[] buf = new byte[bufferSize];

        DatagramPacket receive = new DatagramPacket(buf, bufferSize);

        socket.setSoTimeout(timeout);
        socket.receive(receive);

        String result = new String(receive.getData(),0,receive.getLength());

        JSONObject resultJO = null;
        try {
            resultJO = new JSONObject(result);
        } catch (JSONException e) {
            return null;
        }

        return resultJO;
    }

    private boolean sendMsgToServer(String msg) {
        DatagramPacket sendPacket;

        try {
            sendPacket = new DatagramPacket(msg.getBytes(), msg.getBytes().length, serverIP, serverPort);
            socket.send(sendPacket);
        } catch (IOException e) {
            //TODO
        }

        return true;
    }

    /**
     * Sends a message to the other User
     * @param msg message to Send
     * @return returns true if the sending was successful otherwise not
     */
    @Override
    public synchronized boolean sendMessage(String msg) {
        Log.i(TAG,"Send Msg: "+msg);
        if(state.equals(ConnectionState.DISCONNECTED)) {
            Log.i(TAG,"Send Msg Failed -> State Disconnect");
            return false;
        }

        if(this.comOverServer) {
            String forwardMessage = P2PMessagesUtils.getForwardMessage(this.destIP, this.destPort, msg);
            sendMsgToServer(forwardMessage);
        }
        else {
            DatagramPacket sendPacket;
            try {
                sendPacket = new DatagramPacket(msg.getBytes(), msg.getBytes().length, destIP, destPort);
                socket.send(sendPacket);
            } catch (IOException e) {
                return false;
            }
        }
        Log.i(TAG,"Send Msg Success");
        return true;
    }

    /**
     * Add a P2PCommunicationListener
     * @param listener
     */
    @Override
    public void addReceiveP2PListener(IP2PComListener listener) {
        receiveHandler.addListenerToHandler(listener);
    }

    /**
     * Remove a P2PCommunicationListener
     * @param listener
     */
    @Override
    public void removeReceiveP2PListener(IP2PComListener listener) {
        receiveHandler.removeListenerToHandler(listener);
    }

    private class ReceiveHandler extends Thread {

        public Handler mHandler;

        public final List<IP2PComListener> listeners;

        private final Object mutex = new Object();

        private P2PCommunication com;

        private volatile boolean isTerminated = false;

        public ReceiveHandler(P2PCommunication from) {
            listeners = Collections.synchronizedList(new ArrayList<IP2PComListener>());
            this.com = from;
        }

        private void sendConnectOK() {
            String msgPeer = P2PMessagesUtils.getConnectOKMessage(queryID);
            sendMessage(msgPeer);
        }

        private void sendCheckConnection() {
            String connectMsg= P2PMessagesUtils.getAskConnectMessage(queryID);
            sendMessage(connectMsg);
        }

        @Override
        public void run() {
            Looper.prepare();
            mHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    Bundle bundle = msg.getData();
                    boolean connectionLost = bundle.getString(CONNECTION_LOST_TAG)!=null;
                    boolean connectionEstablished =  bundle.getString(CONNECTION_ESTABLISHED_TAG) != null;
                    boolean sendCheckMsg = bundle.getString(CHECK_CONNECTION_TAG) != null;


                    if(connectionLost) {
                        synchronized (mutex) {
                            for(IP2PComListener listener : listeners ) {
                                listener.onConnectionLost(com);
                            }
                        }
                        return;
                    } else if(connectionEstablished) {
                        synchronized (mutex) {
                            for(IP2PComListener listener : listeners ) {
                                listener.onConnectionEstablished(com);
                            }
                        }
                        return;
                    } else if(sendCheckMsg) {
                        if(!isTerminated)
                            sendCheckConnection();
                        return;
                    }

                    String msgToHandle = bundle.getString(RECEIVE_MSG_TAG);

                    Log.i(TAG,"Handle Message: "+msgToHandle);

                    JSONObject msgToHandleJ = null;
                    String type = "";
                    try {
                        msgToHandleJ = new JSONObject(msgToHandle);
                        type = msgToHandleJ.getString(P2PMessagesUtils.TYPE_KEY);
                    } catch (JSONException e) {
                        synchronized (mutex) {
                            for(IP2PComListener listener : listeners ) {
                                listener.onMessageReceived(msgToHandle,com);
                            }
                        }
                        return;
                    }

                    synchronized (mutex) {
                        for(IP2PComListener listener : listeners ) {
                            if(type != null && type.equals(P2PMessagesUtils.TYPE_CONNECT) && !isTerminated) {
                                sendConnectOK();
                            } else {
                                listener.onMessageReceived(msgToHandle,com);
                            }

                        }
                    }

                }
            };
            Looper.loop();
        }

        public synchronized void stopHandlerLoop() {
            if(!isTerminated) {
                isTerminated=true;
                synchronized (mutex) {
                    for (IP2PComListener list : listeners) {
                        list.onConnectionTermination();
                    }
                }
            }
        }

        public void addListenerToHandler(IP2PComListener listener) {
            synchronized (mutex) {
                listeners.add(listener);
            }
        }

        public void removeListenerToHandler(IP2PComListener listener) {
            synchronized (mutex) {
                listeners.remove(listener);
            }
        }


    }

    private void sendMsgToHandler(String tag,String msgIN) {
        Message msg =  new Message();
        Bundle bundle = new Bundle();
        bundle.putString(tag, msgIN);
        msg.setData(bundle);
        receiveHandler.mHandler.sendMessage(msg);
    }


    private class Receiver extends Thread {

        private volatile boolean isRunning = true;

        private DatagramSocket recvSocket;


        public Receiver(DatagramSocket ds) {
            recvSocket = ds;
            try {
                recvSocket.setSoTimeout(SOCKET_TIMEOUT_RECEIVER);
            } catch (SocketException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            int noMsgReceived = 0;
            while (isRunning) {
                String msg;
                try {
                    msg = listenForMessage();
                    noMsgReceived = 0;
                    if(isRunning)
                        sendMsgToHandler(RECEIVE_MSG_TAG, msg);

                } catch (IOException e) {
                    noMsgReceived++;
                    if(!checkConnection(noMsgReceived)) {
                        StopReceive();
                        break;
                    }
                }
            }
        }

        private boolean checkConnection(int count) {
            if(count>CONNECTION_LOST_ROUNDS) {
                sendMsgToHandler(CONNECTION_LOST_TAG,"");
                return false;
            }

            if(count%2==0) {
                sendMsgToHandler(CHECK_CONNECTION_TAG, "");
            }

            return true;
        }

        public void StopReceive() {
            isRunning=false;
        }

        private String listenForMessage() throws IOException {
            byte[] buf = new byte[bufferSize];
            DatagramPacket receive = new DatagramPacket(buf, bufferSize);
            recvSocket.receive(receive);
            String result = new String(receive.getData(),0,receive.getLength());
            return result;
        }

    }
}
