package com.dsteam.commune.communication;

import android.content.Context;
import android.util.Log;

import com.dsteam.commune.communication.model.BroadcastMessage;
import com.dsteam.commune.communication.model.Message;
import com.dsteam.commune.communication.model.UnicastMessage;
import com.dsteam.commune.communication.peer.IP2PComListener;
import com.dsteam.commune.communication.peer.P2PCommunication;
import com.dsteam.commune.crypto.MessageEncryptor;
import com.dsteam.commune.database.CommuneDatabaseAccess;
import com.dsteam.commune.logic.CommuneLogic;
import com.dsteam.commune.model.User;
import com.dsteam.commune.model.VectorClock;
import com.dsteam.commune.protocol.MessageHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by jpedro on 12.11.14.
 *
 * Class responsible for the whole communication in Commune's distributed system.
 * Coordinates the requests to/from Server, Peer, NFC and GCM.
 */
public class DistributedComm implements IDistributedComm, IP2PComListener {

// CONSTANTS

    private static final String LOG_TAG = DistributedComm.class.getSimpleName();

    private final int THRESHOLD = 3;

// ATTRIBUTES

    private MessageEncryptor mCrypto; // TODO: Should this be a singleton?
    private CommuneDatabaseAccess db;
    private User myUser;
    private CommuneLogic mLogic;


// CONSTRUCTOR

    private static DistributedComm instance = null;

    protected DistributedComm(Context ctxt) {
        db = new CommuneDatabaseAccess(ctxt);
        mLogic = CommuneLogic.getInstance(ctxt);
        try {
            mCrypto = new MessageEncryptor(ctxt);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "Not able to create MessageEncryptor.");
        }
    }

    public static DistributedComm getInstance(Context ctxt) {
        if (instance == null) {
            instance = new DistributedComm(ctxt);
        }
        return instance;
    }

// OPERATIONS

    // Sends message to some random user.
    public boolean unicast(JSONObject message) {
        if (message == null) return false;
        if (myUser == null) myUser = db.getMyUser();

        List<User> possibleDestinations = db.getAllOtherUsers();
        Collections.shuffle(possibleDestinations);
        UnicastMessage unicastMessage = new UnicastMessage(message, myUser, possibleDestinations.get(0), possibleDestinations.subList(1, possibleDestinations.size()));
        Log.d(LOG_TAG + "/unicast", "Sending unicast: " + unicastMessage.toJSON().toString());
        new SenderTask(this, mCrypto).execute(unicastMessage);

        return true;
    }

    // Sends message to given destination.
    public boolean unicast(JSONObject message, User destination) {
        if (message == null) {
            Log.e(LOG_TAG + "/unicast", "Message was null");
            return false;
        }
        if (myUser == null) myUser = db.getMyUser();

        UnicastMessage unicastMessage = new UnicastMessage(message, myUser, destination);
        Log.d(LOG_TAG + "/unicast", "Sending unicast: " + unicastMessage.toJSON().toString());
        new SenderTask(this, mCrypto).execute(unicastMessage);

        return true;
    }

    public boolean unicast(JSONObject message, User destination, P2PCommunication connection) {
        if (connection == null || message == null) {
            Log.e(LOG_TAG + "/unicast", "Connection was null or message was null");
            return false;
        }
        if (myUser == null) myUser = db.getMyUser();
        UnicastMessage unicastMessage = new UnicastMessage(message, myUser, destination);
        Log.d(LOG_TAG + "/unicast", "Sending unicast: " + unicastMessage.toJSON().toString());
        try {
            String encryptedMessage = mCrypto.encryptMessage(unicastMessage.toString());
            boolean successful = connection.sendMessage(encryptedMessage);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean broadcast(JSONObject message) {
        return broadcast(message, db.getAllOtherUsers());
    }

    @Override
    // Chooses and send to log(n) Peers a broadcast message, specifying for each destination which peers
    // should be reached through them.
    public boolean broadcast(JSONObject message, List<User> broadcastPeers) {
        if (myUser == null) myUser = db.getMyUser();
        if(broadcastPeers == null || broadcastPeers.isEmpty()) {
            Log.d(LOG_TAG, "There are no peers to broadcast the message to.");
            return false;
        } else if(broadcastPeers.size() <= THRESHOLD) {
            for (User p : broadcastPeers) {
                BroadcastMessage broadMsg = new BroadcastMessage(message, myUser, p, null);
                Log.d(LOG_TAG + "/broadcast", "Sending broadcast: " + broadMsg.toJSON().toString());
                new SenderTask(this, mCrypto).execute(broadMsg);
            }
            return true;
        }

        // shuffle users to create a new random tree
        Collections.shuffle(broadcastPeers);

        int numberOfDestinationPeers = (int) (Math.log10(broadcastPeers.size()) / Math.log10(2)); //  = log2(numberOfPeers) rounded down

        List<User> destinations = broadcastPeers.subList(0, numberOfDestinationPeers);

        List<List<User>> listOfDescendants = new ArrayList<List<User>>(numberOfDestinationPeers);

        int whichDestinationIndex = 0;

        // round robin distribution of descendants
        for(User p : broadcastPeers.subList(numberOfDestinationPeers, broadcastPeers.size())) {
            int index = whichDestinationIndex % numberOfDestinationPeers;
            List<User> descs = listOfDescendants.get(index);
            if(descs == null) descs = new ArrayList<User>();
            descs.add(p);
            listOfDescendants.set(index, descs);

            whichDestinationIndex++;
        }

        if(destinations.size() == listOfDescendants.size()) {
            Log.e(LOG_TAG, "Destination list size: " + destinations.size() + " is different than descendants list size: " +listOfDescendants.size());
            return false;
        }

        List<BroadcastMessage> broadcastMessages = new ArrayList<BroadcastMessage>();
        int index = 0;

        for(User dest : destinations) {
            broadcastMessages.add(new BroadcastMessage(message, myUser, dest, listOfDescendants.get(index)));
            index++;
        }

        for(BroadcastMessage msg : broadcastMessages) {
            Log.d(LOG_TAG + "/broadcast", "Sending broadcast: " + msg.toJSON().toString());
            new SenderTask(this, mCrypto).execute(msg);
        }

        return true;
    }


    // TODO: NOT REALLY NEEDED, I THINK
    @Override
    public void forward(BroadcastMessage broadcastMessage) {
        broadcast(broadcastMessage.getMessage(), broadcastMessage.getDescendants());
    }

    @Override
    public void onConnectionEstablished(P2PCommunication connection) {

    }

    @Override
    public void onMessageReceived(String encryptedMsg, P2PCommunication connection) {
        String decryptedMsg;
        JSONObject msgAsJson = null;
        JSONObject msgContent;
        List<String> descendantIDs = null;
        String sourceID;
        String type;
        boolean hasGap = false;

        Log.d(LOG_TAG +"/onMessageReceived", "Received encrypted message: " + encryptedMsg);

        // decrypt received message
        try {
            decryptedMsg = mCrypto.decryptMessage(encryptedMsg);
            Log.d(LOG_TAG +"/onMessageReceived", "Decrypted message: " + decryptedMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "Could not decrypt message: " + encryptedMsg);
            return;
        }

        // interpret message as JSON
        try {
            msgAsJson = new JSONObject(decryptedMsg);
            Log.d(LOG_TAG +"/onMessageReceived", "Interpreted as JSON: " + msgAsJson.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "Could not interpret, as JSON, decrypted message: " + decryptedMsg);
            return;
        }

        try {
            msgContent = Message.parseMessageFromJSON(msgAsJson);
            sourceID = Message.parseSourceFromJSON(msgAsJson);
            type = Message.parseTypeFromJSON(msgAsJson);
            if (type.equals(MessageHelper.Broadcast.TYPE)) descendantIDs = BroadcastMessage.parseDescendantsFromJSON(msgAsJson);
            VectorClock myClock = db.getMyVectorClock();
            VectorClock otherClock = VectorClock.fromJSON(msgContent.getJSONObject(MessageHelper.VECTOR_CLOCK_KEY));
            hasGap = myClock.hasNegativeGap(otherClock);
            Log.d(LOG_TAG +"/onMessageReceived", "received vector clock: " + otherClock.toJSON().toString());
            Log.d(LOG_TAG +"/onMessageReceived", "my vector clock: " + myClock.toJSON().toString());
            Log.d(LOG_TAG +"/onMessageReceived", "hasGap? " + hasGap);
            mLogic.handleMessage(msgContent, connection);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        // forward message
        if (type.equals(MessageHelper.Broadcast.TYPE)) {
            broadcast(msgContent, db.getUsersByListOfIDs(descendantIDs));
        }

        if (hasGap) {
            mLogic.requestUpdate(db.getUserByID(sourceID));
            Log.d(LOG_TAG +"/onMessageReceived", "Requested updated from: " + sourceID);
        }
        connection.terminateConnection();
    }

    @Override
    public void onConnectionTermination() {

    }

    @Override
    public void onConnectionLost(P2PCommunication connection) {
        Log.e(LOG_TAG + "/onConnectionLost", "Connection was lost!");
        connection.terminateConnection();
    }



}
