package com.dsteam.commune.communication.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.dsteam.commune.communication.DistributedComm;
import com.dsteam.commune.communication.peer.IP2PComListener;
import com.dsteam.commune.communication.peer.P2PComException;
import com.dsteam.commune.communication.peer.P2PCommunication;
import com.dsteam.commune.communication.peer.P2PMessagesUtils;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 *
 */
// todo: register CommuneLogic somewhere as P2PCommListener
public class GCMIntentService extends IntentService implements IP2PComListener {

    CountDownLatch barrier = new CountDownLatch(1);

    private static long WAIT_TIMEOUT = 50000;

    public GCMIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);
        Log.i("log", "Received: " + extras.toString());

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                Log.i("Tag","Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                Log.i("Tag", "Deleted messages on server: " +
                        extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                String payload = extras.getString("data");


                String type,clientIP,clientPort,queryKey;
                int portTOSend;
                P2PCommunication com = null;
                try {

                    type = extras.getString(P2PMessagesUtils.TYPE_KEY);
                    clientIP = extras.getString(P2PMessagesUtils.CLIENT_IP_KEY);

                    clientPort = extras.getString(P2PMessagesUtils.CLIENT_PORT_KEY);
                    queryKey = extras.getString(P2PMessagesUtils.MSG_ID_KEY);
                    portTOSend = Integer.parseInt(clientPort);

                    if(type==null ||clientIP==null || clientPort==null || queryKey==null) {
                        Log.i("log", "Some payload fields are missing" + payload);
                        return;
                    }


                    if(!type.equals(P2PMessagesUtils.TYPE_ASK_IP)) {
                        Log.i("log", "Wrong message type" + type);
                        return;
                    }


                    com = new P2PCommunication(0);
                    com.addReceiveP2PListener(DistributedComm.getInstance(this));
                    com.addReceiveP2PListener(this);

                    InetAddress ip = InetAddress.getByName(clientIP);

                    com.connect(ip,portTOSend,queryKey);

                    Log.i("log","WaitBarrier");
                    barrier.await(WAIT_TIMEOUT, TimeUnit.MILLISECONDS);
                    Log.i("log","ReleaseBarrier");

                } catch (UnknownHostException e) {
                    Log.i("log", "Msg ip not found "+ payload);
                    return;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (P2PComException e) {
                    Log.i("log", "Connection failed");
                } finally {
                    if(com != null)
                        com.terminateConnection();
                }

            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GCMBroadcastReceiver.completeWakefulIntent(intent);
    }

    @Override
    public void onConnectionEstablished(P2PCommunication connection) {
        Log.i("log","Connection established");
    }

    @Override
    public void onMessageReceived(String msg, P2PCommunication connection) {
        Log.i("log","Msg Received: " + msg);
    }

    @Override
    public void onConnectionLost(P2PCommunication connection) {
    }

    @Override
    public void onConnectionTermination() {
        Log.i("log","Connection down");
        barrier.countDown();
    }
}

