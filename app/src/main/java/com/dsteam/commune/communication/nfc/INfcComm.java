package com.dsteam.commune.communication.nfc;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by jpedro on 11.11.14.
 */
public interface INfcComm {

    public void sendInvitation(Activity a,String transferFileNames[]);

    public void accept(Intent incomingIntent);

}
