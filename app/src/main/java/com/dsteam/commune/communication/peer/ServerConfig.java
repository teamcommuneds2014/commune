package com.dsteam.commune.communication.peer;

/**
 * Created by lukas on 23.11.14.
 */
public class ServerConfig {

    /**
     * IP address of the server
     */
    public static final String SERVER_IP = "77.58.125.170";

    /**
     * Port of the Server
     */
    public static final int SERVER_PORT = 10200;

}
