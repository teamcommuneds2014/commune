package com.dsteam.commune.communication.model;

import android.util.Log;

import com.dsteam.commune.model.User;
import com.dsteam.commune.protocol.MessageHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpedro on 14.12.14.
 */
public class UnicastMessage extends Message {

// CONSTANTS

    private static final String LOG_TAG = UnicastMessage.class.getSimpleName();

// ATTRIBUTES

    private List<User> retryUsers;

// CONSTRUCTOR

    public UnicastMessage(JSONObject msg, User src, User dest, List<User> someRetryUsers) {
        message = msg;
        source = src;
        destination = dest;
        if (someRetryUsers == null) retryUsers = new ArrayList<User>();
        else retryUsers = someRetryUsers;
    }

    public UnicastMessage(JSONObject msg, User src, User dest) {
        message = msg;
        source = src;
        destination = dest;
        retryUsers = new ArrayList<User>();
    }

// RETRY METHODS

    @Override
    public UnicastMessage tryNext() {
        if (retryUsers == null || retryUsers.isEmpty()) return null;
        return new UnicastMessage(message, source, retryUsers.get(0), retryUsers.subList(1,retryUsers.size()));
    }

// FORMAT METHODS

    @Override
    // { type: "unicast", source: 2, destination: 7, message: { ... } }
    public JSONObject toJSON() {
        JSONObject unicastJson = new JSONObject();

        try {
            unicastJson.put(MessageHelper.Message.TYPE_KEY, MessageHelper.Unicast.TYPE);
            unicastJson.put(MessageHelper.Message.SOURCE_KEY, source.getUserID());
            unicastJson.put(MessageHelper.Message.DESTINATION_KEY, destination.getUserID());
            unicastJson.put(MessageHelper.Message.MESSAGE_KEY, message);
        } catch(JSONException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "Some problem with JSON.");
        }

        return unicastJson;
    }
}
