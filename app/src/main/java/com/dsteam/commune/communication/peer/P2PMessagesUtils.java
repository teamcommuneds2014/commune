package com.dsteam.commune.communication.peer;

/**
 * Created by lukas on 21.11.14.
 */

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;

/**
 * Defines constants and functions for the predefined messages of the server and the P2P connection establishment
 * @author lukas
 *
 */
public class P2PMessagesUtils {

    public static final String TYPE_KEY = "type";

    public static final String TYPE_ASK_IP = "ASK_IP";

    public static final String TYPE_CLIENT_ADDR = "CLIENT_IP";

    public static final String TYPE_FORWARDING = "FORWARD";

    public static final String MSG_ID_KEY = "queryID";

    public static final String NAME_KEY = "nameGCM";

    public static final String CLIENT_IP_KEY = "clientIP";

    public static final String CLIENT_PORT_KEY = "clientPort";

    public static final String TYPE_PUNCH_HOLE = "PUNCH_HOLE";

    public static final String MSG_FORWARD_KEY = "message";

    public static final String TYPE_CONNECT = "CONNECT";

    public static final String TYPE_CONNECT_OK = "CONNECT_OK";

    public static String getPunchHoleMessage() {
        JSONObject msg = new JSONObject();
        try {
            msg.put(TYPE_KEY, TYPE_PUNCH_HOLE);
        } catch(JSONException e) {
            return "";
        }
        return msg.toString();
    }

    public static String getToServerAskIPMessage(String gcmName, String queryID) {
        JSONObject msg = new JSONObject();
        try {
            msg.put(TYPE_KEY, TYPE_ASK_IP);
            msg.put(NAME_KEY, gcmName);
            msg.put(MSG_ID_KEY, queryID);
        } catch(JSONException e) {
            return "";
        }
        return msg.toString();
    }

    public static String getAskConnectMessage(String queryID) {
        JSONObject msg = new JSONObject();
        try {
            msg.put(TYPE_KEY, TYPE_CONNECT);
            msg.put(MSG_ID_KEY, queryID);
        } catch(JSONException e) {
            return "";
        }
        return msg.toString();
    }

    public static String getConnectOKMessage(String queryID) {
        JSONObject msg = new JSONObject();
        try {
            msg.put(TYPE_KEY, TYPE_CONNECT_OK);
            msg.put(MSG_ID_KEY, queryID);
        } catch(JSONException e) {
            return "";
        }
        return msg.toString();
    }

    public static String getClientIPMessage(String queryID) {
        JSONObject msg = new JSONObject();
        try {
            msg.put(TYPE_KEY, TYPE_CLIENT_ADDR);
            msg.put(MSG_ID_KEY, queryID);
        } catch(JSONException e) {
            return "";
        }
        return msg.toString();
    }


    public static String getForwardMessage(InetAddress destIP, int destPort, String msg) {
        String destIPS = destIP.toString().substring(1);
        JSONObject json = new JSONObject();
        try {
            json.put(TYPE_KEY, TYPE_FORWARDING);
            json.put(CLIENT_IP_KEY, destIPS);
            json.put(CLIENT_PORT_KEY, destPort);
            json.put(MSG_FORWARD_KEY, msg);
        } catch(JSONException e) {
            return "";
        }
        return json.toString();
    }

    private static int queryNumber = (int) Math.random() * 1000000;

    public static String getNewQueryKey() {
        String key = "queryKey";
        queryNumber++;
        return key+queryNumber;
    }
}

