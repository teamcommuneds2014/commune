package com.dsteam.commune.communication.model;

import com.dsteam.commune.model.User;
import com.dsteam.commune.protocol.MessageHelper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jpedro on 14.12.14.
 */
public abstract class Message {

// ATTRIBUTES

    protected User source;
    protected User destination;
    protected JSONObject message;

// GETTERS

    public JSONObject getMessage() {
        return message;
    }

    public User getSource() {
        return source;
    }

    public User getDestination() {
        return destination;
    }

// RETRY METHODS

    public abstract Message tryNext();

// FORMAT METHODS

    public abstract JSONObject toJSON();

// PARSER METHODS

    public static String parseSourceFromJSON(JSONObject msg) throws JSONException {
        return msg.getString(MessageHelper.Message.SOURCE_KEY);
    }

    public static String parseDestinationFromJSON(JSONObject msg) throws JSONException {
        return msg.getString(MessageHelper.Message.DESTINATION_KEY);
    }

    public static String parseTypeFromJSON(JSONObject msg) throws JSONException {
        return msg.getString(MessageHelper.Message.TYPE_KEY);
    }

    public static JSONObject parseMessageFromJSON(JSONObject msg) throws JSONException {
        return msg.getJSONObject(MessageHelper.Message.MESSAGE_KEY);
    }
}
