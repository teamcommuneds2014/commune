package com.dsteam.commune.communication.model;

import android.util.Log;

import com.dsteam.commune.model.User;
import com.dsteam.commune.protocol.MessageHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpedro on 08.12.14.
 */
public class BroadcastMessage extends Message {

// CONSTANTS

    private static final String LOG_TAG = BroadcastMessage.class.getSimpleName();

// ATTRIBUTES

    private JSONObject message;
    private User source;
    private User destination;           // subtree root
    private List<User> descendants;     // subtree nodes without root

// CONSTRUCTORS

    public BroadcastMessage(JSONObject msg, User src, User dest, List<User> desc) {
        message = msg;
        source = src;
        destination = dest;
        if (desc == null) descendants = new ArrayList<User>();
        else descendants = desc;
    }

// GETTERS

    public JSONObject getMessage() {
        return message;
    }

    public User getSource() {
        return source;
    }

    public User getDestination() {
        return destination;
    }

    public List<User> getDescendants() {
        return descendants;
    }

// RETRY METHODS

    // Used if destination is offline: take first descendant as new destination and remove it from the descendants list.
    public BroadcastMessage tryNext() {
        if (descendants == null || descendants.isEmpty()) {
            return null;
        } else return new BroadcastMessage(message, source, descendants.get(0), descendants.size() > 1 ? descendants.subList(1, descendants.size()) : new ArrayList<User>());
    }

// FORMAT METHODS

    // { type: "broadcast", source: 2, destination: 7, descendants: [3, 5, 6], message: { ... } }
    public JSONObject toJSON() {
        JSONObject broadcastJson = new JSONObject();

        try {
            broadcastJson.put(MessageHelper.Message.TYPE_KEY, MessageHelper.Broadcast.TYPE);
            broadcastJson.put(MessageHelper.Message.SOURCE_KEY, source.getUserID());
            broadcastJson.put(MessageHelper.Message.DESTINATION_KEY, destination.getUserID());

            JSONArray descs = new JSONArray();
            for(User d : descendants) {
                descs.put(d.getUserID());
            }

            broadcastJson.put(MessageHelper.Broadcast.DESCENDANTS_KEY, descs);
            broadcastJson.put(MessageHelper.Message.MESSAGE_KEY, message);
        } catch(JSONException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "Some problem with JSON.");
        }

        return broadcastJson;
    }

// PARSER METHODS

    public static List<String> parseDescendantsFromJSON(JSONObject broadMsg) throws JSONException {
        List<String> descList = new ArrayList<String>();

        JSONArray descJSONArray = broadMsg.getJSONArray(MessageHelper.Broadcast.DESCENDANTS_KEY);
        for(int i = 0; i < descJSONArray.length(); i++) {
            descList.add(descJSONArray.getString(i));
        }

        return descList;
    }

}
