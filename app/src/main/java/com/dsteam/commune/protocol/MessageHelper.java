package com.dsteam.commune.protocol;

/**
 * Created by jpedro on 05.12.14.
 */
public class MessageHelper {

    public static final String COMMAND_KEY = "cmd";
    public static final String SENDER_KEY = "sender";
    public static final String VECTOR_CLOCK_KEY = "vector_clock";

    public static final String CREATE_POLL_CMD = "create_poll";
    public static final String REQUEST_UPDATE_CMD = "request_update";
    public static final String RESPONSE_UPDATE_CMD = "response_update";
    public static final String ADD_NEW_USER_CMD = "add_new_user";
    public static final String VOTE_CMD = "vote";

    //key to get the keyValue from sharedPreferences
    public final static String PREF_KEY_NAME = "GroupKey";
    public final static String PREF_GROUP_NAME = "GroupName";
    //file key allows us to identify the shared Prefs
    public final static String PREF_FILE_KEY = "com.dsteam.commune";


    public static class Message {
        public static final String TYPE_KEY = "type";
        public static final String SOURCE_KEY = "source";
        public static final String DESTINATION_KEY = "destination";
        public static final String MESSAGE_KEY = "message";
    }

    public static class Broadcast {
        public static final String TYPE = "broadcast";
        public static final String DESCENDANTS_KEY = "descendants";
    }

    public static class Unicast {
        public static final String TYPE = "unicast";
    }

    public static class UpdateResponse {
        public static final String USERS_KEY = "users";
        public static final String POLLS_KEY = "polls";
        public static final String VOTES_KEY = "votes";
    }

    public static class Poll {

        public static final String POLL_LABEL = "poll";

        public static final String QUESTION_KEY = "question";

        public static final String OPTIONS_KEY = "options";

        public static final String POLL_ID_KEY = "poll_id";

        public static final String POLL_USER_KEY = "poll_user";

    }

    public static class PollOption {

        public static final String OPTION_KEY = "option";

        public static final String OPTION_ID_KEY = "option_id";

        public static final String POLL_ID_KEY = "poll_id";

        public static final String POLL_USER_KEY = "poll_user";
    }

    public static class PollVote {

        public static final String VOTE_LABEL = "vote";

        public static final String VOTER_ID_KEY = "voter_id";

        public static final String OPTION_ID_KEY = "option_id";

        public static final String POLL_ID_KEY = "poll_id";

        public static final String POLL_USER_KEY = "poll_user";

    }

    public static class User {

        public static final String USER_LABEL = "user";

        public static final String USER_ID_KEY = "user_id";

        public static final String GCM_ID_KEY = "gcm_id";

        public static final String USERNAME_KEY = "username";
    }

    public static class Timestamp {

        public static final String TIMESTAMP_LABEL = "timestamp";

        public static final String CLOCK_KEY = "clock";
        public static final String USER_ID_KEY = "user_id";
    }
}
