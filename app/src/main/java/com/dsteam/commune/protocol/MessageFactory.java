package com.dsteam.commune.protocol;

import android.util.Log;

import com.dsteam.commune.model.Poll;
import com.dsteam.commune.model.PollVote;
import com.dsteam.commune.model.User;
import com.dsteam.commune.model.VectorClock;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by jpedro on 05.12.14.
 */
public class MessageFactory {

    public static final String LOG_TAG = MessageFactory.class.getSimpleName();

    // { cmd: 'request_update', sender: 123, vector_clock: {"user_id" : "value", ... } }
    public static JSONObject getRequestUpdateMessage(String myUserID, VectorClock myClock) {
        JSONObject msg = new JSONObject();

        try {
            msg.put(MessageHelper.COMMAND_KEY, MessageHelper.REQUEST_UPDATE_CMD);
            msg.put(MessageHelper.SENDER_KEY, myUserID);
            msg.put(MessageHelper.VECTOR_CLOCK_KEY, myClock.toJSON());
        } catch(JSONException e) {
            e.printStackTrace();
            Log.e(LOG_TAG + "/getRequestUpdateMessage", "Some problem with JSON.");
        }

        return msg;
    }

    // { cmd: 'response_update', sender: 124, vector_clock: {"user_id" : "value", ... }, polls: [], users: [], votes: [] }
    public static JSONObject getResponseUpdateMessage(List<Poll> polls, List<User> users, List<PollVote> votes, String myUserID, VectorClock myClock) {
        JSONObject msg = new JSONObject();

        try {
            msg.put(MessageHelper.COMMAND_KEY, MessageHelper.RESPONSE_UPDATE_CMD);
            msg.put(MessageHelper.SENDER_KEY, myUserID);
            msg.put(MessageHelper.VECTOR_CLOCK_KEY, myClock.toJSON());

            JSONArray pollsJson = new JSONArray();
            for(Poll p : polls) {
                pollsJson.put(p.toJSON());
            }

            JSONArray usersJson = new JSONArray();
            for(User u : users) {
                usersJson.put(u.toJSON());
            }

            JSONArray votesJson = new JSONArray();
            for(PollVote v : votes) {
                votesJson.put(v.toJSON());
            }

            msg.put(MessageHelper.UpdateResponse.POLLS_KEY, pollsJson);
            msg.put(MessageHelper.UpdateResponse.USERS_KEY, usersJson);
            msg.put(MessageHelper.UpdateResponse.VOTES_KEY, votesJson);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(LOG_TAG + "/getResponseUpdateMessage", "Some problem with JSON.");
        }

        return msg;
    }

    // { cmd: 'create_poll', sender: 123, vector_clock: {"user_id": "value", ... },
    //   poll: { question: "lunch?", poll_id: 2, poll_user: 123, timestamp: { user_id: 123, clock: 78 },
    //           options: [{ option: "no", option_id: 1 }, { option: "yes", ... }] } }
    public static JSONObject getCreatePollMessage(Poll newPoll, String myUserID, VectorClock myClock) {
        JSONObject msg = new JSONObject();

        try {
            msg.put(MessageHelper.COMMAND_KEY, MessageHelper.CREATE_POLL_CMD);
            msg.put(MessageHelper.SENDER_KEY, myUserID);
            msg.put(MessageHelper.VECTOR_CLOCK_KEY, myClock.toJSON());

            msg.put(MessageHelper.Poll.POLL_LABEL, newPoll.toJSON());
        } catch(JSONException e) {
            e.printStackTrace();
            Log.e(LOG_TAG + "/getCreatePollMessage", "Some problem with JSON.");
        }

        return msg;
    }

    // { cmd: 'vote', sender: 123, vector_clock: {"user_id": "value", ... },
    //   vote: { voter_id: 123, poll_id: 2, poll_user: 123, option_id: 1, timestamp: { user_id: 123, clock: 3 }} }
    public static JSONObject getVoteMessage(PollVote vote, String myUserID, VectorClock myClock) {
        JSONObject msg = new JSONObject();

        try {
            msg.put(MessageHelper.COMMAND_KEY, MessageHelper.VOTE_CMD);
            msg.put(MessageHelper.SENDER_KEY, myUserID);
            msg.put(MessageHelper.VECTOR_CLOCK_KEY, myClock.toJSON());


            msg.put(MessageHelper.PollVote.VOTE_LABEL, vote.toJSON());
        } catch(JSONException e) {
            e.printStackTrace();
            Log.e(LOG_TAG + "/getVoteMessage", "Some problem with JSON.");
        }

        return msg;
    }

    // { cmd: 'add_new_user', sender: 123, vector_clock: {"user_id": "value", ... },
    //   user: { user_id: 124, gcm_id: ... , username: "marc", timestamp: { user_id: 123, clock: 2 } } }
    public static JSONObject getAddNewUserMessage(User newUser, String myUserID, VectorClock myClock) {
        JSONObject msg = new JSONObject();

        try {
            msg.put(MessageHelper.COMMAND_KEY, MessageHelper.ADD_NEW_USER_CMD);
            msg.put(MessageHelper.SENDER_KEY, myUserID);
            msg.put(MessageHelper.VECTOR_CLOCK_KEY, myClock.toJSON());

            msg.put(MessageHelper.User.USER_LABEL, newUser.toJSON());
        } catch(JSONException e) {
            e.printStackTrace();
            Log.e(LOG_TAG + "/getAddNewUserMessage", "Some problem with JSON.");
        }

        return msg;
    }
}
