package com.dsteam.commune.logic;

import android.app.Activity;
import android.content.Intent;

import com.dsteam.commune.model.FeedObject;
import com.dsteam.commune.model.Poll;
import com.dsteam.commune.model.PollVote;
import com.dsteam.commune.model.Task;
import com.dsteam.commune.model.User;

import java.util.List;

/**
 * Created by jpedro on 11.11.14.
 *
 * Representation of a Commune.
 * Interface responsible with communicating with DistributedComm and DatabaseHelper.
 * Receives requests from Activities.
 *
 */
public interface ICommuneLogic{

    public void requestUpdate();

    public boolean isInGroup();

    public List<FeedObject> getFeedObjects();

    public Poll getPoll(int pollID, String pollUserID);

    public void createNewGroup(String groupName);

    public void registerMyUser(String username, String gcmID);

    public String getCurrentGroupName();

    public User getOwnUser();

    public List<User> getUsers();

    public Poll createPoll(String question, List<String> options);

    // TODO: FIND SIGNATURE
    public Poll vote(PollVote pollVote);

    public void invite(Activity activity);

    public void acceptInvitation(Activity a, Intent incomingIntent);

    public Task createTask(String task);

    public interface OnDataChangedListener {
        public void onPollDataChanged(int pollID, String pollUserID);
        public void onUserDataChanged();
        public void onFeedDataChanged();
    }
}
