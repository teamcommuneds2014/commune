package com.dsteam.commune.logic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.dsteam.commune.communication.DistributedComm;
import com.dsteam.commune.communication.nfc.NfcComm;
import com.dsteam.commune.communication.nfc.NfcHelper;
import com.dsteam.commune.communication.peer.P2PCommunication;
import com.dsteam.commune.database.CommuneDatabaseAccess;
import com.dsteam.commune.database.DatabaseEntry;
import com.dsteam.commune.model.FeedObject;
import com.dsteam.commune.model.Poll;
import com.dsteam.commune.model.PollOption;
import com.dsteam.commune.model.PollVote;
import com.dsteam.commune.model.Task;
import com.dsteam.commune.model.User;
import com.dsteam.commune.model.VectorClock;
import com.dsteam.commune.protocol.MessageFactory;
import com.dsteam.commune.protocol.MessageHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpedro on 04.12.14.
 */
public class CommuneLogic implements ICommuneLogic {

// CONSTANTS

    private static final String LOG_TAG = CommuneLogic.class.getSimpleName();

// SINGLETON PATTERN

    private static CommuneLogic instance = null;

    protected CommuneLogic(Context ctxt) {
        // 'protected' to defeat instantiation.

        db = new CommuneDatabaseAccess(ctxt);
        mContext = ctxt;
        prefs = mContext.getSharedPreferences(MessageHelper.PREF_FILE_KEY, Context.MODE_PRIVATE);
        mDataListener = new OnDataChangedListener() {
            @Override
            public void onPollDataChanged(int pollID, String pollUserID) {

            }

            @Override
            public void onUserDataChanged() {

            }

            @Override
            public void onFeedDataChanged() {

            }
        };
    }

    public static CommuneLogic getInstance(Context ctxt) {
        if (instance == null) {
            instance = new CommuneLogic(ctxt);
        }
        return instance;
    }

    public void init() {
        mDistributedComm = DistributedComm.getInstance(mContext);
        myUser = db.getMyUser();
    }

// ATTRIBUTES

    private Context mContext;

    private DistributedComm mDistributedComm;

    private CommuneDatabaseAccess db;

    private User myUser;

    private NfcComm nfcComm;

    private SharedPreferences prefs;

    private OnDataChangedListener mDataListener;

// INTERFACE METHODS

    @Override
    public boolean isInGroup() {
        String groupName = prefs.getString(MessageHelper.PREF_GROUP_NAME, null);
        Log.d(LOG_TAG + "/isInGroup", groupName == null ? "GroupName is null" : "GroupName is: "+ groupName);
        return groupName != null;
    }

    @Override
    public List<FeedObject> getFeedObjects() {
        // get polls
        List<Poll> pollsWithoutVoters = db.getAllPolls();
        // List<Task> tasks = db.getAllTasks();
        List<FeedObject> feedObjectList = new ArrayList<FeedObject>();

        for (Poll poll : pollsWithoutVoters) {
            feedObjectList.add(getPollWithVoters(poll));
        }

        return feedObjectList;
    }

    @Override
    public Poll getPoll(int pollID, String pollUserID) {
        Poll poll = db.getPoll(pollUserID, pollID);
        return getPollWithVoters(poll);
    }

    private Poll getPollWithVoters(Poll pollWithoutVoters) {
        if (pollWithoutVoters == null) return null;

        Poll pollWithVoters = new Poll(pollWithoutVoters.getQuestion(),
                                       pollWithoutVoters.getPollUserID(),
                                       pollWithoutVoters.getTimestamp().vectorClockEntry);

        List<PollOption> optionList = pollWithoutVoters.getOptions();
        if (optionList == null || optionList.isEmpty()) {
            Log.d(LOG_TAG + "/getPollWithVoters", "Poll didn't have any PollOptions!");
            return pollWithoutVoters;
        }

        for(PollOption opt : optionList) {
            List<PollVote> pollVotes = db.getPollVotes(opt);
            PollOption optionWithVoters = new PollOption(opt.getOption(), opt.getPollID(), opt.getOptionID(), opt.getPollUserID());
            if (pollVotes != null && !pollVotes.isEmpty()) {
                List<User> voters = new ArrayList<User>();
                for (PollVote pollVote : pollVotes) {
                    String voterID = pollVote.getVoterID();
                    voters.add(db.getUserByID(voterID));
                }
                optionWithVoters.setVoters(voters);
                Log.d(LOG_TAG + "/getPollWithVoters", "Adding voters to PollOption: " + optionWithVoters);
            }
            pollWithVoters.addOption(optionWithVoters);
        }

        Log.d(LOG_TAG + "/getPollWithVoters", pollWithVoters.toString());
        return pollWithVoters;
    }

    @Override
    public void requestUpdate() {
        if(mDistributedComm == null || myUser == null) init();
        JSONObject updateMsg = MessageFactory.getRequestUpdateMessage(getOwnUser().getUserID(), db.getMyVectorClock());
        mDistributedComm.unicast(updateMsg);
    }

    // TODO: WHAT ABOUT RETRYUSERS WITH FIXED DESTINATION?
    public void requestUpdate(User destination) {
        if(mDistributedComm == null || myUser == null) init();
        JSONObject updateMsg = MessageFactory.getRequestUpdateMessage(getOwnUser().getUserID(), db.getMyVectorClock());
        mDistributedComm.unicast(updateMsg, destination);
    }

    public void requestUpdate(User destination, P2PCommunication connection) {
        if (mDistributedComm == null) {
            init();
            connection.addReceiveP2PListener(mDistributedComm);
        }
        JSONObject updateMsg = MessageFactory.getRequestUpdateMessage(getOwnUser().getUserID(), db.getMyVectorClock());
        mDistributedComm.unicast(updateMsg, destination, connection);
    }

    @Override
    public void createNewGroup(String groupName) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(MessageHelper.PREF_GROUP_NAME, groupName);
        editor.commit();
    }

    @Override
    public void registerMyUser(String username, String gcmID) {
        WifiManager manager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        String userID = info.getMacAddress();
        db.setUpMyUser(username, userID, gcmID);
        myUser = db.getMyUser();
    }

    @Override
    public String getCurrentGroupName() {
        return prefs.getString(MessageHelper.PREF_GROUP_NAME, null);
    }

    public User getOwnUser() {
        if (myUser == null) myUser = db.getMyUser();
        return myUser;
    }

    @Override
    public List<User> getUsers() {
        List<User> users = db.getAllUsers();

        for (User user : users) {
            Log.d(LOG_TAG + "/getUsers", user.toString());
        }

        return users;
    }

    public Poll createPoll(String question, List<String> options) {
        if(mDistributedComm == null || myUser == null) init();
        Poll newPoll = db.createPoll(question, options);

        JSONObject createPollMessage = MessageFactory.getCreatePollMessage(newPoll, getOwnUser().getUserID(), db.getMyVectorClock());
        mDistributedComm.broadcast(createPollMessage);

        return newPoll;
    }

    @Override
    public Poll vote(PollVote pollVote) {
        if(mDistributedComm == null || myUser == null) init();
        PollVote voted = db.createPollVote(pollVote.getPollID(), pollVote.getPollUserID(), pollVote.getOptionID());
        JSONObject voteMessage = MessageFactory.getVoteMessage(voted, voted.getVoterID(), db.getMyVectorClock());
//        Poll updatedPoll = db.getPoll(voted.getPollUserID(), voted.getPollID());

        Poll updatedPoll = getPoll(voted.getPollID(), voted.getPollUserID());

        mDistributedComm.broadcast(voteMessage);

        return updatedPoll;
    }

    //attention: currently task is not handled
    @Override
    public void invite(Activity activity) {
        if(mDistributedComm == null || myUser == null) init();

        nfcComm = new NfcComm(mContext);

        if(nfcComm.isNfcCompatible()) {
            String transferFileNames[] = {NfcHelper.groupFileName, NfcHelper.userFileName, NfcHelper.pollFileName, NfcHelper.voteFileName};

            List<User> users = db.getAllUsers();
            List<Poll> polls = db.getAllPolls();
            List<PollVote> votes = db.getAllPollVotes();

            //get the key from sharedPreferences to send
            String stringKey = prefs.getString(MessageHelper.PREF_KEY_NAME, "");
            //get the group name from prefs
            String groupName = prefs.getString(MessageHelper.PREF_GROUP_NAME, "");

            //create files
            createFileFromDatabase(users, NfcHelper.userFileName);
            createFileFromDatabase(polls, NfcHelper.pollFileName);
            createFileFromDatabase(votes, NfcHelper.voteFileName);
            createFileFromStrings(new String[]{stringKey, groupName}, NfcHelper.groupFileName);

            //send files via nfc
            nfcComm.sendInvitation(activity, transferFileNames);

        }else { //TODO warn user that nfc is not available
        }


    }

    //this weird list definition allows to use  polymorphism
    private void createFileFromDatabase(List<? extends DatabaseEntry> entries, String transferFileName)
    {

            File root = android.os.Environment.getExternalStorageDirectory();

            File dir = new File (root.getAbsolutePath() + "/download");
            dir.mkdirs();

            File file = new File(dir, transferFileName);


            try {
                FileOutputStream f = new FileOutputStream(file);
                PrintWriter pw = new PrintWriter(f);
                for(DatabaseEntry entry : entries) {

                    pw.println(entry.toJSON().toString());
                    pw.flush();
                }

                pw.close();
                f.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.i("DatabaseToFile", "******* File not found. Did you" +
                        " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


    }

    //writes the groupKey into the file
    private void createFileFromStrings(String strings[], String transferFileName)
    {
        File root = android.os.Environment.getExternalStorageDirectory();

        File dir = new File (root.getAbsolutePath() + "/download");
        dir.mkdirs();
        File file = new File(dir, transferFileName);

        try {
            FileOutputStream f = new FileOutputStream(file);
            PrintWriter pw = new PrintWriter(f);
            for(String string : strings) {
                Log.d(LOG_TAG + "/createFileFromStrings", "wrote line: " + string + " to " + transferFileName);
                pw.println(string);
                pw.flush();
            }
            pw.close();
            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.i("DatabaseToFile", "******* File not found. Did you" +
                    " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void acceptInvitation(Activity a, Intent incomingIntent) {

        nfcComm = new NfcComm(mContext);
        String filePath;

        if(nfcComm.isNfcCompatible())
        {
            nfcComm.accept(incomingIntent);
            filePath = nfcComm.getFilePath();

            if(filePath != null) {

                //cache test
                Log.d("Files", "In Cache");
                File f = mContext.getCacheDir().getAbsoluteFile();
                File file[] = f.listFiles();
                Log.d("Files", "Size: "+ file.length);
                for (File aFile : file) {
                    Log.d("Files", "FileName:" + aFile.getName());
                }

                //insert our newly received data into the db
                writeFileToDatabase(filePath + "/" + NfcHelper.userFileName, NfcHelper.tableNames.USER);
                writeFileToDatabase(filePath + "/" + NfcHelper.pollFileName, NfcHelper.tableNames.POLL);
                writeFileToDatabase(filePath + "/" + NfcHelper.voteFileName, NfcHelper.tableNames.POLLVOTE);
                //insert key to shared prefs
                writeGroupDataToPrefs(filePath + "/" + NfcHelper.groupFileName);


                 //TODO inform the user to prepare for android beam
                //now the receiver sends his user data
                sendUserData(a);

            }else
            {
                Log.e(LOG_TAG + "/acceptInvitation", "filePath was null");
                //TODO warn the user, that there was a problem with android beam
            }

        }else
        {
            //TODO warn user that nfc is not available
        }


    }

    private void writeFileToDatabase(String fileName, NfcHelper.tableNames name)
    {
        File inputFile = new File(fileName);
        if(inputFile.exists()) {
            FileInputStream fileIn;
            String dataRow;

            try {
                fileIn = new FileInputStream(inputFile);
                BufferedReader myReader = new BufferedReader(
                        new InputStreamReader(fileIn));

                while ((dataRow = myReader.readLine()) != null) {

                    switch (name) {
                        case USER:
                            User user = User.fromJSON(new JSONObject(dataRow));
                            List<User> userAsList = new ArrayList<User>();
                            userAsList.add(user);
                            Log.d(LOG_TAG + "/writeFileToDatabase", "Adding user (from file): " + user.toString());
                            db.insertAllMissingUsers(userAsList);
                            break;
                        case POLL:
                            Poll poll = Poll.fromJSON(new JSONObject(dataRow));
                            List<Poll> pollAsList = new ArrayList<Poll>();
                            pollAsList.add(poll);
                            Log.d(LOG_TAG + "/writeFileToDatabase", "Adding poll (from file): " + poll.toJSON().toString());
                            db.insertAllMissingPolls(pollAsList);
                            break;
                        case POLLVOTE:
                            PollVote pollVote = PollVote.fromJSON(new JSONObject(dataRow));
                            List<PollVote> voteAsList = new ArrayList<PollVote>();
                            voteAsList.add(pollVote);
                            Log.d(LOG_TAG + "/writeFileToDatabase", "Adding vote (from file): " + pollVote.toJSON().toString());
                            db.insertAllMissingPollVotes(voteAsList);
                            break;
                        default:
                            Log.e(LOG_TAG + "/writeFileToDatabase", "Not handling data properly!! Default case should not be reachable!");
                            break;
                    }

                    Log.d(LOG_TAG+ "/writeFileToDatabase", "Finished reading file: " + fileName);

                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            inputFile.delete();

        } else {
            Log.e(LOG_TAG + "/writeFileToDatabase", fileName + " does not exist!");
        }

    }

    //this function writes the string key and groupkey into shared preferences
    private void writeGroupDataToPrefs(String fileName)
    {
        File inputFile = new File(fileName);
        if(inputFile.exists()) {
            FileInputStream fileIn;
            int dataSize = 2;
            String[] groupData = new String[dataSize];

            try {
                fileIn = new FileInputStream(inputFile);
                BufferedReader myReader = new BufferedReader(
                        new InputStreamReader(fileIn));

                for (int i = 0; i < dataSize; i++) {
                    Log.d(LOG_TAG + "/writeGroupDataToPrefs", "wrote data: " + groupData[i] + " to prefs");
                    groupData[i] = myReader.readLine();

                }

                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(MessageHelper.PREF_KEY_NAME, groupData[0]);
                editor.apply();
                editor.putString(MessageHelper.PREF_GROUP_NAME, groupData[1]);
                editor.apply();
                editor.commit();

                String groupName = prefs.getString(MessageHelper.PREF_GROUP_NAME, "");
                Log.d(LOG_TAG, groupName == null ? "no group added" : "added group: " + groupName);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }

            inputFile.delete();
        }
    }

    //the newly invited member sends his MAC address , username and userid back to the sender of the invitation
    private void sendUserData(Activity a) {

        if(myUser == null)
            myUser = db.getMyUser();

        String userName = getOwnUser().getUsername();
        String userID = getOwnUser().getUserID();
        String gcmID = getOwnUser().getGcmID();
        createFileFromStrings(new String[]{userName, userID, gcmID}, NfcHelper.localUserFileName);


        nfcComm.sendUserData(a, NfcHelper.localUserFileName);

    }

    //this method is called if the MAC address arrives at the invitation sender
    public void onUserDataReceived(Intent incomingIntent)
    {
        if(mDistributedComm == null) init();
        nfcComm = new NfcComm(mContext);
        String filePath;

        nfcComm.accept(incomingIntent);
        filePath = nfcComm.getFilePath();

        //write the new user to my db and broadcast new user message to all other users
        if(filePath != null) {

            User invitedUser = writeUserDataToDb(filePath + "/" + NfcHelper.localUserFileName);
            if(invitedUser != null) {
                JSONObject addNewUserMsg = MessageFactory.getAddNewUserMessage(invitedUser, getOwnUser().getUserID(), db.getMyVectorClock());
                mDistributedComm.broadcast(addNewUserMsg);
            }

        }else
        {
            //TODO warn the user, that there was a problem with android beam
        }

    }

    //return the newly added nuser
    private User writeUserDataToDb(String fileName)
    {
        User invitedUser = null;
        File inputFile = new File(fileName);
        if(inputFile.exists()) {
            FileInputStream fileIn;
            int dataSize = 3;
            String[] userData = new String[dataSize];


            try {
                fileIn = new FileInputStream(inputFile);
                BufferedReader myReader = new BufferedReader(
                        new InputStreamReader(fileIn));

                for (int i = 0; i < dataSize; i++) {

                    userData[i] = myReader.readLine();

                }

                invitedUser = db.createUser(userData[0], userData[1], userData[2]);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }

            inputFile.delete();
        }
        return invitedUser;

    }

    @Override
    public Task createTask(String task) {
        return null;
    }

// SETTERS

    public void setDataListener(OnDataChangedListener listener) {
        mDataListener = listener;
    }

// MESSAGE HANDLING

    public void handleMessage(JSONObject message, P2PCommunication connection) {
        if (message == null) {
            Log.e(LOG_TAG + "/handleMessage", "Message was null");
            return;
        }
        Log.d(LOG_TAG + "/handleMessage", "Handling message: " + message.toString());
        String messageType = "";
        try {
            messageType = message.getString(MessageHelper.COMMAND_KEY);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(LOG_TAG + "/handleMessage", "No command key found in message: " + message.toString());
        }

        if (messageType.equals(MessageHelper.CREATE_POLL_CMD)) {
            handleCreatePollMessage(message);
        } else if (messageType.equals(MessageHelper.ADD_NEW_USER_CMD)){
            handleAddNewUserMessage(message);
        } else if (messageType.equals(MessageHelper.VOTE_CMD)) {
            handleVoteMessage(message);
        } else if (messageType.equals(MessageHelper.RESPONSE_UPDATE_CMD)) {
            handleResponseUpdateMessage(message);
        } else if (messageType.equals(MessageHelper.REQUEST_UPDATE_CMD)) {
            handleRequestUpdateMessage(message, connection);
        }
    }

    private void handleCreatePollMessage(JSONObject createPollMsg) {
        try {
            Poll receivedPoll = Poll.fromJSON(createPollMsg.getJSONObject(MessageHelper.Poll.POLL_LABEL));
            db.insertPoll(receivedPoll);
            mDataListener.onFeedDataChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleVoteMessage(JSONObject voteMsg) {
        try {
            PollVote receivedVote = PollVote.fromJSON(voteMsg.getJSONObject(MessageHelper.PollVote.VOTE_LABEL));
            Log.d(LOG_TAG + "/handleVoteMessage", "Handling vote: " + receivedVote.toString());
            db.insertPollVote(receivedVote);
            mDataListener.onPollDataChanged(receivedVote.getPollID(), receivedVote.getPollUserID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleAddNewUserMessage(JSONObject addNewUserMsg) {
        try {
            User receivedUser = User.fromJSON(addNewUserMsg.getJSONObject(MessageHelper.User.USER_LABEL));
            db.insertUser(receivedUser);
            mDataListener.onUserDataChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleResponseUpdateMessage(JSONObject updateMsg) {
        JSONArray pollsJSON;
        JSONArray votesJSON;
        JSONArray usersJSON;

        try {
            pollsJSON = updateMsg.getJSONArray(MessageHelper.UpdateResponse.POLLS_KEY);
            votesJSON = updateMsg.getJSONArray(MessageHelper.UpdateResponse.VOTES_KEY);
            usersJSON = updateMsg.getJSONArray(MessageHelper.UpdateResponse.USERS_KEY);

            List<Poll> polls = new ArrayList<Poll>();
            List<PollVote> votes = new ArrayList<PollVote>();
            List<User> users = new ArrayList<User>();

            for (int i = 0; i < pollsJSON.length(); i++) {
                Poll newPoll = Poll.fromJSON(pollsJSON.getJSONObject(i));
                polls.add(newPoll);
            }
            if(!polls.isEmpty()) {
                db.insertAllMissingPolls(polls);
                mDataListener.onFeedDataChanged();
            }
            for (int i = 0; i < votesJSON.length(); i++) {
                PollVote newVote = PollVote.fromJSON(votesJSON.getJSONObject(i));
                votes.add(newVote);
            }
            if (!votes.isEmpty()) {
                db.insertAllMissingPollVotes(votes);
            }
            for(PollVote vote : votes) {
                mDataListener.onPollDataChanged(vote.getPollID(), vote.getPollUserID());
            }
            for (int i = 0; i < usersJSON.length(); i++) {
                users.add(User.fromJSON(usersJSON.getJSONObject(i)));
            }
            if (!users.isEmpty()) {
                db.insertAllMissingUsers(users);
                mDataListener.onUserDataChanged();
            }
        } catch(JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleRequestUpdateMessage(JSONObject requestMsg, P2PCommunication connection) {
        if(mDistributedComm == null || myUser == null) init();
        try {
            VectorClock lastVectorClock = VectorClock.fromJSON(requestMsg.getJSONObject(MessageHelper.VECTOR_CLOCK_KEY));
            List<Poll> missingPolls = db.getAllMissingPolls(lastVectorClock);
            List<User> missingUsers = db.getAllMissingUsers(lastVectorClock);
            List<PollVote> missingVotes = db.getAllMissingPollVotes(lastVectorClock);

            JSONObject updateResponse = MessageFactory.getResponseUpdateMessage(missingPolls, missingUsers, missingVotes, getOwnUser().getUserID(), db.getMyVectorClock());
            User destination = db.getUserByID(requestMsg.getString(MessageHelper.SENDER_KEY));

            mDistributedComm.unicast(updateResponse, destination);
        } catch(JSONException e){
            e.printStackTrace();
        }
    }

}
