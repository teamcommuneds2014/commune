package com.dsteam.commune.graphics;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by march_000 on 30.11.2014.
 */
public class PollView extends View {
    float mWidth, mHeight;
    RowData[] mRowData;


    public PollView(Context context, AttributeSet attrs) {
            super(context, attrs);

         mRowData = new RowData[3];
        mRowData[0]= new RowData("test1", 3);
        mRowData[1]= new RowData("test1", 2);
        mRowData[2]= new RowData("test1", 4);


    }

    public void setRowData(RowData[] newData){
        mRowData = newData;
        invalidate();
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mWidth = widthMeasureSpec;
        mHeight = heightMeasureSpec;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawPaint(paint);
        paint.setColor(Color.BLACK);

        paint.setTextSize(50);


        int maxValue = 0;
        for(int i = 0; i<mRowData.length; i++){
            if(mRowData[i].value>maxValue)maxValue=mRowData[i].value;
        }
        float sideStep = mWidth/(mRowData.length*2-1);
        float currentOffset = 0;

        for(int i = 0; i<mRowData.length; i++) {
            canvas.drawRect(currentOffset, mHeight*mRowData[i].value/maxValue, 0, currentOffset+sideStep, paint);
            currentOffset+=sideStep*2;
        }



        /*
        canvas.rotate(-90);
        canvas.drawText("Test", -50, -50, paint);
        canvas.restore();*/
    }

    public class RowData{
        public RowData(String name, int value){
            this.name = name;
            this.value = value;
        }
        public String name;
        public int value;
    }

}
