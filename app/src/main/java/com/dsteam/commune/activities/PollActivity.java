package com.dsteam.commune.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dsteam.commune.R;
import com.dsteam.commune.adapters.PollOptionsAdapter;
import com.dsteam.commune.logic.CommuneLogic;
import com.dsteam.commune.logic.ICommuneLogic;
import com.dsteam.commune.model.Poll;
import com.dsteam.commune.model.PollOption;

public class PollActivity extends Activity {
    public final static String POLLIDSTRING = "poll_ID";
    public final static String POLLUSERIDSTRING = "poll_user_ID";

    ListView pollOptionsListView;
    TextView title;
    CommuneLogic mCommuneLogic;
    Poll mCurrentPoll;
    int mPollID;
    String mPollUserID;
    Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poll_view);
        title = (TextView)findViewById(R.id.poll_activity_title);
        pollOptionsListView = (ListView)findViewById(R.id.poll_options_list);
        mPollID = getIntent().getIntExtra(POLLIDSTRING, -1);
        mPollUserID = getIntent().getStringExtra(POLLUSERIDSTRING);
        mCommuneLogic = CommuneLogic.getInstance(this);
        if(mPollID!=-1||mPollUserID!=null) updateView();
        else {
            Toast.makeText(this, "fuck you, I quit", Toast.LENGTH_SHORT).show();
            finish();
        }
        mCommuneLogic.setDataListener(new ICommuneLogic.OnDataChangedListener() {
            @Override
            public void onPollDataChanged(int pollID, String pollUserID) {
                if(mPollID==pollID&&pollUserID.equals(mPollUserID))
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            updateView();
                        }
                    }, 100);
            }

            @Override
            public void onUserDataChanged() {

            }

            @Override
            public void onFeedDataChanged() {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateView();
    }


    public boolean hasVoted;

    public void updateView(){
        //mCommuneLogic.requestUpdate();
        mCurrentPoll = mCommuneLogic.getPoll(mPollID, mPollUserID);
        hasVoted = mCurrentPoll.hasVoted(mCommuneLogic.getOwnUser());
        title.setText(mCurrentPoll.getQuestion());
        pollOptionsListView.setAdapter(new PollOptionsAdapter(this, mCurrentPoll.getOptions().toArray(
                new PollOption[mCurrentPoll.getNumberOfOptions()])));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.poll_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
