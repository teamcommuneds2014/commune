package com.dsteam.commune.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.dsteam.commune.R;
import com.dsteam.commune.logic.CommuneLogic;
import com.dsteam.commune.logic.ICommuneLogic;

public class NewGroupActivity extends Activity {

    public static final String USERNAMESTRING = "username";
    public static final String GCMIDSTRING = "gcm_ID";
    ICommuneLogic mCommuneLogic;
    String mUsername;
    String mGCMID;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group);
        mCommuneLogic = CommuneLogic.getInstance(this);
        mUsername = getIntent().getStringExtra(USERNAMESTRING);
        mGCMID = getIntent().getStringExtra(GCMIDSTRING);
        if(mUsername==null||mGCMID==null)finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.new_group, menu);
        return true;
    }



    public void formGroup(View view){
        String groupName = ((TextView)findViewById(R.id.group_name_text)).getText().toString();
        mCommuneLogic.createNewGroup(groupName);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
