package com.dsteam.commune.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.dsteam.commune.R;
import com.dsteam.commune.adapters.UsersAdapter;
import com.dsteam.commune.logic.CommuneLogic;
import com.dsteam.commune.logic.ICommuneLogic;
import com.dsteam.commune.model.User;

import java.util.List;

public class UsersActivity extends Activity {

    ListAdapter mAdapter;
    CommuneLogic mCommuneLogic;
    ListView mUserListView;
    Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        mCommuneLogic = CommuneLogic.getInstance(this);
        List<User> userList = mCommuneLogic.getUsers();
        mAdapter = new UsersAdapter(this, userList.toArray(new User[userList.size()]));
        mUserListView = (ListView)findViewById(R.id.users_list);
        mUserListView.setAdapter(mAdapter);
        mCommuneLogic.setDataListener(new ICommuneLogic.OnDataChangedListener() {
            @Override
            public void onPollDataChanged(int pollID, String pollUserID) {

            }

            @Override
            public void onUserDataChanged() {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        updateView();
                    }
                }, 100);
            }

            @Override
            public void onFeedDataChanged() {

            }
        });
    }

    void updateView(){
        List<User> userList = mCommuneLogic.getUsers();
        mUserListView.setAdapter(new UsersAdapter(
                this, userList.toArray(new User[userList.size()])));
    }

    public void inviteNewUser(View v) {

        Intent intent = new Intent(this, ActiveNFCJoin.class);
        startActivity(intent);
    }
}
