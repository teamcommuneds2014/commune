package com.dsteam.commune.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

import com.dsteam.commune.R;
import com.dsteam.commune.logic.CommuneLogic;
import com.dsteam.commune.logic.ICommuneLogic;

import java.util.ArrayList;
import java.util.List;

public class NewPollActivity extends Activity {

    List<View> optionETs = new ArrayList<View>();
    ViewGroup mContainer;
    ScrollView mScrollView;
    Handler mHandler = new Handler();
    ICommuneLogic mCommuneLogic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCommuneLogic = CommuneLogic.getInstance(this);
        setContentView(R.layout.activity_new_poll);
        mContainer = (ViewGroup)findViewById(R.id.votes_container);
        mScrollView = (ScrollView)findViewById(R.id.new_poll_scrollview);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.new_poll, menu);
        return true;
    }

    public void submitPoll(View view){
        String pollName = ((EditText)findViewById(R.id.new_poll_activity_title)).getText().toString();
        if(pollName.isEmpty())return;
        List<String> options = new ArrayList<String>();
        for(View view1: optionETs){
            String newString = ((EditText)view1.findViewById(R.id.option_EditText)).getText().toString();
            if(newString!="") options.add(newString);
        }
        if(mCommuneLogic.createPoll(pollName, options)!=null)
            finish();
        else
        {
            Toast.makeText(this, "some problems with database", Toast.LENGTH_SHORT);
        }

    }

    public void deleteOption(View view){
        mContainer.removeView((View)view.getParent());
        optionETs.remove(view.getParent());
    }


    public void addOptionET(View view){
        if(optionETs.size()<10) {
            final View newView = LayoutInflater.from(this).inflate(R.layout.poll_option_item, mContainer, false);
            optionETs.add(newView);
            mContainer.addView(newView);
            mHandler.postDelayed(new Runnable() {
                                     @Override
                                     public void run() {
                                         //newView.requestFocus();
                                         mScrollView.smoothScrollTo(0, mScrollView.getBottom());
                                     }
                                 }, 300
            );
        }
        else {
            Toast.makeText(this, "max 10 options", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
