package com.dsteam.commune.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.dsteam.commune.R;
import com.dsteam.commune.adapters.FeedAdapter;
import com.dsteam.commune.database.CommuneDatabaseHelper;
import com.dsteam.commune.dialogs.FeedObjectDialog;
import com.dsteam.commune.logic.CommuneLogic;
import com.dsteam.commune.logic.ICommuneLogic;
import com.dsteam.commune.model.FeedObject;
import com.dsteam.commune.protocol.MessageHelper;

import java.util.List;


public class MainActivity extends Activity {

    ListView feedListView;
    TextView title;
    CommuneLogic mCommuneLogic;
    Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        feedListView =(ListView)findViewById(R.id.feed_list_view);
        title = (TextView)findViewById(R.id.main_activity_title);
        mCommuneLogic = CommuneLogic.getInstance(this);
        mCommuneLogic.setDataListener(new ICommuneLogic.OnDataChangedListener() {
            @Override
            public void onPollDataChanged(int pollID, String pollUserID) {

            }

            @Override
            public void onUserDataChanged() {

            }

            @Override
            public void onFeedDataChanged() {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        updateView();
                    }
                }, 100);
            }
        });
        goToStart();
        updateView();
        title.setText(mCommuneLogic.getCurrentGroupName());
    }

    private void goToStart(){
        if(!mCommuneLogic.isInGroup()){
            Intent intent = new Intent(this, ConnectActivity.class);
            startActivity(intent);
            finish();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    FeedObjectDialog mFeedObjectDialog;

    public void addFeedObject(View view){
        mFeedObjectDialog = new FeedObjectDialog();
        mFeedObjectDialog.show(getFragmentManager(), "test");
    }

    public void showUsers(View view){
        Intent intent = new Intent(this, UsersActivity.class);
        startActivity(intent);
    }

    public void deleteAll(View view){
        deleteDatabase(CommuneDatabaseHelper.DB_NAME);
        SharedPreferences prefs = getSharedPreferences(MessageHelper.PREF_FILE_KEY, Context.MODE_PRIVATE);
        prefs.edit().clear().commit();
        prefs = getSharedPreferences(MainActivity.class.getSimpleName(),Context.MODE_PRIVATE);
        prefs.edit().clear().commit();
        goToStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateView();
    }

    void updateView(){
        //mCommuneLogic.requestUpdate();
        title.setText(mCommuneLogic.getCurrentGroupName());
        List<FeedObject> feedObjects = mCommuneLogic.getFeedObjects();
        FeedAdapter feedAdapter = new FeedAdapter(this, feedObjects.toArray(new FeedObject[feedObjects.size()]));
        feedListView.setAdapter(feedAdapter);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
