package com.dsteam.commune.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.dsteam.commune.R;
import com.dsteam.commune.communication.gcm.GCMLogin;
import com.dsteam.commune.logic.CommuneLogic;
import com.dsteam.commune.logic.ICommuneLogic;

public class ConnectActivity extends Activity {

    ICommuneLogic mICommuneLogic;
    GCMLogin mGCMLogin;
    EditText mUsernameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mICommuneLogic = CommuneLogic.getInstance(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);
        mGCMLogin = new GCMLogin(this);
        goToMain();
        mUsernameEditText = (EditText)findViewById(R.id.usernameEditText);
    }

    private void addMyEntry() {
        String gcmID = null;
        while((gcmID = mGCMLogin.getGCMID())==null) {
        }
        mICommuneLogic.registerMyUser(mUsernameEditText.getText().toString(),gcmID);
    }

    public void formNewGroup(View view){
        if(mUsernameEditText.getText().toString().isEmpty())return;


        if(mICommuneLogic.getOwnUser()==null) {
            addMyEntry();
        }

        String gcmID = mGCMLogin.getGCMID();
        if(gcmID==null || gcmID.isEmpty()) {
            showGCMErrorDialog();
            return;
        }

        Intent intent = new Intent(this, NewGroupActivity.class);
        intent.putExtra(NewGroupActivity.GCMIDSTRING, gcmID);
        intent.putExtra(NewGroupActivity.USERNAMESTRING, mUsernameEditText.getText().toString());
        startActivity(intent);
    }

    private void goToMain(){
        if(mICommuneLogic.isInGroup()){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void showGCMErrorDialog() {
        new AlertDialog.Builder(this)
                .setTitle("No GCM ID found")
                .setMessage("Please retry after a few seconds")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // no action
                    }
                })
                .show();
    }

    public void joinGroup(View view){
        if(mUsernameEditText.getText().toString().isEmpty())return;
        if(mICommuneLogic.getOwnUser()==null) {
            addMyEntry();
        }

        String gcmID = mGCMLogin.getGCMID();
        if(gcmID==null || gcmID.isEmpty()) {
            showGCMErrorDialog();
            return;
        }

        Intent intent = new Intent(this, passiveJoinHelpActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.connect, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
           }
    return super.onOptionsItemSelected(item);
    }
}
